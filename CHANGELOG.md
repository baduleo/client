Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [[v0.17.0][]] - 2018-07-08
### Commits

- Now address field loads data from geolocation. [`c59b990`][c59b990]
- Importing our index will make us benefit from all the already defined [`cbd21bb`][cbd21bb]
- Minor style change for form error messages [`5422776`][5422776]

[v0.17.0]: https://gitlab.com/baduleo/client/compare/v0.16.2...v0.17.0
[c59b990]: https://gitlab.com/baduleo/client/commit/c59b990e1fa9330d3268fb584471f6f2a313d27e
[cbd21bb]: https://gitlab.com/baduleo/client/commit/cbd21bb297e215d30770b41960bffb8d100da87a
[5422776]: https://gitlab.com/baduleo/client/commit/5422776889339e2c793d4fead7722d5d619dbcc6

## [[v0.16.2][]] - 2018-07-07
### Commits

- Upgraded packages [`3736664`][3736664]
- Updated css-loader [`443c0d2`][443c0d2]

[v0.16.2]: https://gitlab.com/baduleo/client/compare/v0.16.1...v0.16.2
[3736664]: https://gitlab.com/baduleo/client/commit/3736664707e70d21ab00b883768dbbd7012a82de
[443c0d2]: https://gitlab.com/baduleo/client/commit/443c0d256f61154d252dd58e31f5834e045ed979

## [[v0.16.1][]] - 2018-07-07
### Commits

- Just a minor style change [`6767534`][6767534]

[v0.16.1]: https://gitlab.com/baduleo/client/compare/v0.16.0...v0.16.1
[6767534]: https://gitlab.com/baduleo/client/commit/6767534ace65219327582de3b37d800c1053974f

## [[v0.16.0][]] - 2018-07-07
### Commits

- Added form validation errors [`7b034d3`][7b034d3]

[v0.16.0]: https://gitlab.com/baduleo/client/compare/v0.15.0...v0.16.0
[7b034d3]: https://gitlab.com/baduleo/client/commit/7b034d3480d64db5ed1a8f3ceecf2dd6e8491b50

## [[v0.15.0][]] - 2018-07-07
### Commits

- Changes to the geolocation input in the form [`0bac85c`][0bac85c]
- Added webpack-bundle-analyzer [`5b38984`][5b38984]
- Trying something with .gitlab-ci.yml templates.. [`59b700c`][59b700c]
- Added styles for form's textarea [`92e68b4`][92e68b4]
- Minor changes to the not found page [`d2cd566`][d2cd566]
- Fixed staging api url [`395d19b`][395d19b]

[v0.15.0]: https://gitlab.com/baduleo/client/compare/v0.14.6...v0.15.0
[0bac85c]: https://gitlab.com/baduleo/client/commit/0bac85ce51ab98893e0f5cbb99c89d17c94c56df
[5b38984]: https://gitlab.com/baduleo/client/commit/5b389847468fe4fe70c9020b8472cf4352d3d177
[59b700c]: https://gitlab.com/baduleo/client/commit/59b700ca70a0c12d6ad09ff09ce22c65f9ff6e15
[92e68b4]: https://gitlab.com/baduleo/client/commit/92e68b459ac270e9ed0e1494be316305285443f3
[d2cd566]: https://gitlab.com/baduleo/client/commit/d2cd566a43d9e11db47b0b4aa65950948c060e56
[395d19b]: https://gitlab.com/baduleo/client/commit/395d19bb3942ffa2e4d2b8890d3b676306d2da36

## [[v0.14.6][]] - 2018-07-02
### Commits

- Simplified .gitlab-ci.yml file [`04efef6`][04efef6]

[v0.14.6]: https://gitlab.com/baduleo/client/compare/v0.14.5...v0.14.6
[04efef6]: https://gitlab.com/baduleo/client/commit/04efef6809cec6f38df79fac42d9a9805ed059e2

## [[v0.14.5][]] - 2018-07-02
### Commits

- Added missing MAPBOX_TOKEN var [`d0a624c`][d0a624c]
- Removed unnecessary env args [`1470a70`][1470a70]

[v0.14.5]: https://gitlab.com/baduleo/client/compare/v0.14.4...v0.14.5
[d0a624c]: https://gitlab.com/baduleo/client/commit/d0a624c1696159678b0c7a3cb8ffb77318c890f8
[1470a70]: https://gitlab.com/baduleo/client/commit/1470a70b9a695c8bc75615ba1132dee33c917c34

## [[v0.14.4][]] - 2018-07-02
### Commits

- Adding image build process [`2df9022`][2df9022]
- Just added some badges to the readme file [`38116b8`][38116b8]
- Removed unnecessary @types packages [`07c9916`][07c9916]

[v0.14.4]: https://gitlab.com/baduleo/client/compare/v0.14.3...v0.14.4
[2df9022]: https://gitlab.com/baduleo/client/commit/2df90223aee7a76fa36be74043497c6fc31c625f
[38116b8]: https://gitlab.com/baduleo/client/commit/38116b8906071498385ae6c2e31739d31bbf9049
[07c9916]: https://gitlab.com/baduleo/client/commit/07c9916d297db15058ec50ff41528a2f7cec6a76

## [[v0.14.3][]] - 2018-07-02
### Commits

- Coding style fix [`2d31607`][2d31607]

[v0.14.3]: https://gitlab.com/baduleo/client/compare/v0.14.2...v0.14.3
[2d31607]: https://gitlab.com/baduleo/client/commit/2d31607e1fdd20f3df1bc73f1b15d1e4073b6774

## [[v0.14.2][]] - 2018-07-02
### Commits

- Now form properly posts badus [`9be81f4`][9be81f4]
- Style changes [`ad7ca92`][ad7ca92]
- Fixed uncontrolled inputs issue [`aaab2c8`][aaab2c8]

[v0.14.2]: https://gitlab.com/baduleo/client/compare/v0.14.1...v0.14.2
[9be81f4]: https://gitlab.com/baduleo/client/commit/9be81f49ee34a2a531cfeb0b2acf669b2b38f30d
[ad7ca92]: https://gitlab.com/baduleo/client/commit/ad7ca921d06cd51130bf6a0c0c9f10b0b6a10b24
[aaab2c8]: https://gitlab.com/baduleo/client/commit/aaab2c8a42e0d6016cd5a270c4ce421c395df3c1

## [[v0.14.1][]] - 2018-07-01
### Commits

- Ensure form data is removed when closing it [`c770e2a`][c770e2a]

[v0.14.1]: https://gitlab.com/baduleo/client/compare/v0.14.0...v0.14.1
[c770e2a]: https://gitlab.com/baduleo/client/commit/c770e2a44dec9b40150de52e1aede6cbe1956756

## [[v0.14.0][]] - 2018-07-01
### Commits

- Lot of changes related to modal & form [`f3b4ba8`][f3b4ba8]

[v0.14.0]: https://gitlab.com/baduleo/client/compare/v0.13.0...v0.14.0
[f3b4ba8]: https://gitlab.com/baduleo/client/commit/f3b4ba823edf039fe06788db2ebade1b2aa6bc97

## [[v0.13.0][]] - 2018-07-01
### Commits

- Added Form Modal component (it still doesn't work) [`ff108b8`][ff108b8]
- Minor style changes [`0cbb3dc`][0cbb3dc]
- Minor style change [`2907850`][2907850]
- Changed mapbox map style to a lighter one [`0f830ae`][0f830ae]

[v0.13.0]: https://gitlab.com/baduleo/client/compare/v0.12.2...v0.13.0
[ff108b8]: https://gitlab.com/baduleo/client/commit/ff108b851d06b649510be6954f81a223a048b627
[0cbb3dc]: https://gitlab.com/baduleo/client/commit/0cbb3dc71deda9eac867312634344ac2efc4af2d
[2907850]: https://gitlab.com/baduleo/client/commit/2907850fe93da7d7a76fd17b07febcc46762f0cb
[0f830ae]: https://gitlab.com/baduleo/client/commit/0f830aec98ee68548b1393a873b181cba5657b2e

## [[v0.12.2][]] - 2018-07-01
### Commits

- Regenerate mapbox token (and remove it from the code..) [`3d15f19`][3d15f19]

[v0.12.2]: https://gitlab.com/baduleo/client/compare/v0.12.1...v0.12.2
[3d15f19]: https://gitlab.com/baduleo/client/commit/3d15f1970a368994c3c827d59438bfb22404ec22

## [[v0.12.1][]] - 2018-07-01
### Commits

- Re-added .gitlab-ci.yml file, devops is too slow and useless for this project [`100eab0`][100eab0]

[v0.12.1]: https://gitlab.com/baduleo/client/compare/v0.12.0...v0.12.1
[100eab0]: https://gitlab.com/baduleo/client/commit/100eab016f523c4b850b0fa527be031a1919160d

## [[v0.12.0][]] - 2018-07-01
### Commits

- Changed badu fields according to new spec [`5afec4f`][5afec4f]
- Moved Cluster onClick logic to an exported function [`f2dd2fd`][f2dd2fd]
- Converted processResponse into a Promise [`bf6a8b9`][bf6a8b9]
- Mock axios requests in tests [`d0c303c`][d0c303c]
- Created API_URL env var [`26afa88`][26afa88]

[v0.12.0]: https://gitlab.com/baduleo/client/compare/v0.11.1...v0.12.0
[5afec4f]: https://gitlab.com/baduleo/client/commit/5afec4f164fc176ce615df3de5fe45490a5b9fc2
[f2dd2fd]: https://gitlab.com/baduleo/client/commit/f2dd2fd33e9d10cd4704ee22ac0d3f76779ae19c
[bf6a8b9]: https://gitlab.com/baduleo/client/commit/bf6a8b9b0c23fb76350802ea6f6da46d989e5bc0
[d0c303c]: https://gitlab.com/baduleo/client/commit/d0c303c1d90bb56d655f49eae60f1dfb48dd7c48
[26afa88]: https://gitlab.com/baduleo/client/commit/26afa887b2489b875dbdcb8d60ea9b949b13f215

## [[v0.11.1][]] - 2018-07-01
### Commits

- Road to API [`85b3fbb`][85b3fbb]
- Fixed ids mapper method [`54f2359`][54f2359]
- Delete .gitlab-ci.yml (to enable autodevops) [`c236c77`][c236c77]
- Fixed badus api path [`6511f15`][6511f15]

[v0.11.1]: https://gitlab.com/baduleo/client/compare/v0.11.0...v0.11.1
[85b3fbb]: https://gitlab.com/baduleo/client/commit/85b3fbb090eeca817a46985569755b822e571dc4
[54f2359]: https://gitlab.com/baduleo/client/commit/54f2359a8864c5e380ec89be49d15c84d2159d0a
[c236c77]: https://gitlab.com/baduleo/client/commit/c236c7752b7e46752b054f9c873cb0f08f2e9db7
[6511f15]: https://gitlab.com/baduleo/client/commit/6511f15deb53cbe990fcdadfee6c6f3609b53996

## [[v0.11.0][]] - 2018-06-28
### Commits

- Preparing it for the badus form [`a65cff7`][a65cff7]
- Changed button style when filter is active [`e19817c`][e19817c]
- Properly tested the main reducer file [`365fae6`][365fae6]
- Moved map styles to scss file [`3e4a8f4`][3e4a8f4]
- There's no need for a UI folder (at least for now) [`e72b731`][e72b731]

[v0.11.0]: https://gitlab.com/baduleo/client/compare/v0.10.0...v0.11.0
[a65cff7]: https://gitlab.com/baduleo/client/commit/a65cff72227c3a0774d72d94313f1687f15d09d5
[e19817c]: https://gitlab.com/baduleo/client/commit/e19817c385f5396f523c97b22dbfbc7c1d262bff
[365fae6]: https://gitlab.com/baduleo/client/commit/365fae6db0b16cb0e4f90c82d343c6f760001466
[3e4a8f4]: https://gitlab.com/baduleo/client/commit/3e4a8f44585915fd7745b12d8067342c92df2362
[e72b731]: https://gitlab.com/baduleo/client/commit/e72b7319f31e98ffb396da743208675187a6a227

## [[v0.10.0][]] - 2018-06-28
### Commits

- Added toggleClosed button in top of the map. [`0c7f5f7`][0c7f5f7]
- Minor style changes & fixes [`7166942`][7166942]
- Only parse localStorage position if it's defined and string [`a5f5b20`][a5f5b20]
- Minor prop-type fix [`952064f`][952064f]

[v0.10.0]: https://gitlab.com/baduleo/client/compare/v0.9.1...v0.10.0
[0c7f5f7]: https://gitlab.com/baduleo/client/commit/0c7f5f70b7d0c559139c451debb9f9653f173063
[7166942]: https://gitlab.com/baduleo/client/commit/716694220b077b8e2d69698585ff66f036bc003f
[a5f5b20]: https://gitlab.com/baduleo/client/commit/a5f5b201d6e84f823cdfc16d48388d87260f9cb6
[952064f]: https://gitlab.com/baduleo/client/commit/952064f56137167067ffaff22d4625647d1fdaee

## [[v0.9.1][]] - 2018-06-27
### Commits

- Added missing tests + minor fixes [`2b2c023`][2b2c023]

[v0.9.1]: https://gitlab.com/baduleo/client/compare/v0.9.0...v0.9.1
[2b2c023]: https://gitlab.com/baduleo/client/commit/2b2c023cd7e59eaafa0128abd5197195a84a21d4

## [[v0.9.0][]] - 2018-06-27
### Commits

- Added open/closed filters logic [`bd6c06e`][bd6c06e]

[v0.9.0]: https://gitlab.com/baduleo/client/compare/v0.8.3...v0.9.0
[bd6c06e]: https://gitlab.com/baduleo/client/commit/bd6c06ed60e9b0173f14f4fc5071d0ce6fb75c88

## [[v0.8.3][]] - 2018-06-27
### Commits

- Fixed map reducer to work with new geolocation stuff [`170e879`][170e879]

[v0.8.3]: https://gitlab.com/baduleo/client/compare/v0.8.2...v0.8.3
[170e879]: https://gitlab.com/baduleo/client/commit/170e87982edca87b7eb3acf61b87013f5afda531

## [[v0.8.2][]] - 2018-06-27
### Commits

- Increase commit limit in changelog file [`c94c98f`][c94c98f]

[v0.8.2]: https://gitlab.com/baduleo/client/compare/v0.8.1...v0.8.2
[c94c98f]: https://gitlab.com/baduleo/client/commit/c94c98f4d336bc0cede74667a5baed7ddab2bebd

## [[v0.8.1][]] - 2018-06-27
### Commits

- Fix auto-changelog template [`e3cd75d`][e3cd75d]

[v0.8.1]: https://gitlab.com/baduleo/client/compare/v0.8.0...v0.8.1
[e3cd75d]: https://gitlab.com/baduleo/client/commit/e3cd75de6dff5393b1c2728d0c872233cb3cae75

## [[v0.8.0][]] - 2018-06-27
### Commits

- Rewriten entire geolocation stuff [`1def25b`][1def25b]
- Fixed some tests... [`4aaaa2a`][4aaaa2a]
- Remove changelog updates from changelog file [`ef75227`][ef75227]

[v0.8.0]: https://gitlab.com/baduleo/client/compare/v0.7.0...v0.8.0
[1def25b]: https://gitlab.com/baduleo/client/commit/1def25b3ec390fc4a7c9dbe6b241ea8949136ac7
[4aaaa2a]: https://gitlab.com/baduleo/client/commit/4aaaa2a04cb22859425c51676b4617c7ac475bd8
[ef75227]: https://gitlab.com/baduleo/client/commit/ef75227a04f234b3498990a35b15533ba273e0a9

## [[v0.7.0][]] - 2018-06-26
### Commits

- Added react-redux-geolocation [`d4d8a85`][d4d8a85]
- Added bootstrap (only basics) [`bd98fcb`][bd98fcb]
- Updated readme file [`66f017e`][66f017e]
- Center to geolocation when detected [`e05fd20`][e05fd20]
- Added CLUSTER_EXENT var [`83a1392`][83a1392]
- Changed h3 to h6 for Popups' titles [`16f2c26`][16f2c26]
- Added missing routerRedoucer to redoucers index [`08dfe81`][08dfe81]
- Minor note about the first ifs in Cluster component [`6a87e16`][6a87e16]
- Minor coding style change [`8aa860f`][8aa860f]
- Fixed missing key prop in Cluster [`65ca34e`][65ca34e]

[v0.7.0]: https://gitlab.com/baduleo/client/compare/v0.6.1...v0.7.0
[d4d8a85]: https://gitlab.com/baduleo/client/commit/d4d8a85d4446a6c073bebe03e4bd37e4af108d96
[bd98fcb]: https://gitlab.com/baduleo/client/commit/bd98fcbd4f386efa07367a060b484b6a40aea774
[66f017e]: https://gitlab.com/baduleo/client/commit/66f017eab4d6618a2c3410655e77df94f9833b97
[e05fd20]: https://gitlab.com/baduleo/client/commit/e05fd20a3f5bae32ddd1187413b7d7214a1c5db9
[83a1392]: https://gitlab.com/baduleo/client/commit/83a1392e3ffed1ebca400613085c411dc3a01d76
[16f2c26]: https://gitlab.com/baduleo/client/commit/16f2c26fde9ab159923625f1337db4aab8687888
[08dfe81]: https://gitlab.com/baduleo/client/commit/08dfe810978542a1b33f486cb3d55f7a29972ae2
[6a87e16]: https://gitlab.com/baduleo/client/commit/6a87e165917047ad447124398cddc36d774a2c74
[8aa860f]: https://gitlab.com/baduleo/client/commit/8aa860ff45b3deb7179fb3697623bdfe2624c522
[65ca34e]: https://gitlab.com/baduleo/client/commit/65ca34ebe1392461ee4ec908556f9401892e04fd

## [[v0.6.1][]] - 2018-06-24
### Commits

- Minor changes related to time formatting [`42a5320`][42a5320]

[v0.6.1]: https://gitlab.com/baduleo/client/compare/v0.6.0...v0.6.1
[42a5320]: https://gitlab.com/baduleo/client/commit/42a532010cc72c9b6b88250363ab52d0b4169d40

## [[v0.6.0][]] - 2018-06-24
### Commits

- Center and zoom-in on Cluster click [`56d829b`][56d829b]
- Minor style change to changelog file [`c26ff83`][c26ff83]

[v0.6.0]: https://gitlab.com/baduleo/client/compare/v0.5.0...v0.6.0
[56d829b]: https://gitlab.com/baduleo/client/commit/56d829bcb989facec8cf1adf1a8d22cb1486c772
[c26ff83]: https://gitlab.com/baduleo/client/commit/c26ff83dfe1df981aac0321cb5a6995409cc82bc

## [[v0.5.0][]] - 2018-06-24
### Commits

- Added audo-changelog [`7c7340b`][7c7340b]

[v0.5.0]: https://gitlab.com/baduleo/client/compare/v0.4.0...v0.5.0
[7c7340b]: https://gitlab.com/baduleo/client/commit/7c7340b0059defbf4fdc17e4cef00bdae0b85363

## [[v0.4.0][]] - 2018-06-24
### Commits

- Now Clusters have different sizes depending on the amount of points [`c8b5f47`][c8b5f47]
- Added FALLBACK_LOCATION variable [`6244459`][6244459]
- Added time formatting method [`2f7a13e`][2f7a13e]
- Added really basic Mapbox.js test (cannot mock gl for now..) [`051ff69`][051ff69]
- Only zoom-in if zoom < ZOOM_ON_POPUP [`9c09223`][9c09223]
- Fix center of markers [`2bf0a3e`][2bf0a3e]

[v0.4.0]: https://gitlab.com/baduleo/client/compare/v0.3.0...v0.4.0
[c8b5f47]: https://gitlab.com/baduleo/client/commit/c8b5f47e348f59d1215d32c3ca31c4a1ba47c79c
[6244459]: https://gitlab.com/baduleo/client/commit/6244459451361f5e2579d15abc23e700f4da5c80
[2f7a13e]: https://gitlab.com/baduleo/client/commit/2f7a13e24e5d81e6bd6ae9db64d035dd7b16e187
[051ff69]: https://gitlab.com/baduleo/client/commit/051ff694fd359ad5794ed694033ee80e7b34440e
[9c09223]: https://gitlab.com/baduleo/client/commit/9c092238d164433fbccde52e1ae0a4bcab7e7286
[2bf0a3e]: https://gitlab.com/baduleo/client/commit/2bf0a3ee4c0568e020f290dc8e5b7b8afc8c84a8

## v0.3.0 - 2018-06-24
### Commits

- Added cluster, markers and a lot of more shit [`b0b865d`][b0b865d]
- Badus are now loaded (from a json) to redux store [`39cbbfa`][39cbbfa]
- Added postversion script [`bcc9203`][bcc9203]
- Added .gitlab-ci.yml (for now only tests) [`0d0e9a7`][0d0e9a7]
- Set required node version to 8 (just in case) [`19b3ad3`][19b3ad3]
- I cannot find a way to test index.js, so I'll ignore it by now [`347c2ea`][347c2ea]
- Testing improvements [`139624a`][139624a]
- Added react-router-redux [`19ab278`][19ab278]
- Added editorconfig file [`8f91a18`][8f91a18]
- Added custom eslint ruleset [`a044148`][a044148]

[b0b865d]: https://gitlab.com/baduleo/client/commit/b0b865d070023da4fbbe6ea62e2ca5f0750508de
[39cbbfa]: https://gitlab.com/baduleo/client/commit/39cbbfab69edb465617f3e26af924658d272dcc4
[bcc9203]: https://gitlab.com/baduleo/client/commit/bcc92034e5f123ec9171a122cc8127da32eac44e
[0d0e9a7]: https://gitlab.com/baduleo/client/commit/0d0e9a77198c964e9888ec2d14e6dd17d6728cf8
[19b3ad3]: https://gitlab.com/baduleo/client/commit/19b3ad3905a03673f0a74c05f3e87ed6ed11a210
[347c2ea]: https://gitlab.com/baduleo/client/commit/347c2ea659411adc7826266ffe9887f7c5e698ad
[139624a]: https://gitlab.com/baduleo/client/commit/139624aad6038dba3377fc58ad8e9decda3d41b8
[19ab278]: https://gitlab.com/baduleo/client/commit/19ab278038fdef9b3398424d4de8e2fbfd6c9343
[8f91a18]: https://gitlab.com/baduleo/client/commit/8f91a18dc0c28bf15e76eda16f7ec14af384dd05
[a044148]: https://gitlab.com/baduleo/client/commit/a0441482e1bea31c5aee5f8ac7580d53749d8076
