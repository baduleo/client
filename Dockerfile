FROM node:8

LABEL maintainer="Òscar Casajuana <elboletaire@underave.net>"

ARG API_URL
ARG MAPBOX_TOKEN

ENV API_URL $API_URL
ENV MAPBOX_TOKEN $MAPBOX_TOKEN

RUN mkdir /app-src /app

WORKDIR /app-src

COPY . /app-src

RUN yarn && yarn run build && \
    mv -f build/* /app && \
    rm -fr /app-src

WORKDIR /app

RUN npm install --global static-server

ENTRYPOINT [ "static-server", "-p", "5000" ]
