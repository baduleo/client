import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

configure({ adapter: new Adapter() })

global.URL.createObjectURL = jest.fn()
global.gl = {
  getExtension: jest.fn(),
}

export const baduEntity = (props) => ({
  id: 1,
  name: 'Pasteleria Bolleria Isabel',
  slug: 'pasteleria-bolleria-isabel',
  from: '06:00:00',
  to: '23:00:00',
  address: 'Carrer del Doctor Bov\u00e9, 125, 08032 Barcelona, Espanya',
  lat: 41.4208330225,
  lng: 2.160349745,
  deletedAt: null,
  createdAt: new Date('2011-08-09 16:21:27'),
  updatedAt: new Date('2011-08-09 16:21:27'),
  ...props,
})
