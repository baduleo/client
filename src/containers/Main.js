import { connect } from 'react-redux'

import Main from '../components/Pages/Main'

const MainContainer = connect(
  (state) => ({
    badus: state.badus,
    map: state.map,
    geolocation: state.geolocation,
    form: state.form,
  })
)(Main)

export default MainContainer
