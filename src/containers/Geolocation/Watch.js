import { connect } from 'react-redux'

import Geolocation from '../../components/Geolocation'
import { watch, watchSuccess, watchError } from '../../actions/geolocation'

const Watch = connect(
  () => ({}),
  (dispatch) => ({
    init: () => dispatch(watch()),
    success: (payload) => dispatch(watchSuccess(payload)),
    error: (error) => dispatch(watchError(error)),
  })
)(Geolocation)

export default Watch
