import { connect } from 'react-redux'

import Geolocation from '../../components/Geolocation'
import { locate, locateSuccess, locateError } from '../../actions/geolocation'

const Locate = connect(
  () => ({}),
  (dispatch) => ({
    init: () => dispatch(locate()),
    success: (payload) => dispatch(locateSuccess(payload)),
    error: (error) => dispatch(locateError(error)),
  })
)(Geolocation)

export default Locate
