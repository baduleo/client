/*
 * Main (random) constants
 */

export const MAPBOX_API = 'https://api.mapbox.com'
export const MAPBOX_API_PLACES = `${MAPBOX_API}/geocoding/v5/mapbox.places`

export const DEFAULT_ZOOM = 10
export const ZOOM_ON_POPUP = 16
export const FALLBACK_LOCATION = [
  // We ❤️ Barcelona
  2.1794408,
  41.3980716,
]
// no. of points: class to be used when no. of points is surpassed
export const CLUSTER_SIZES = {
  20: 'bigger',
  100: 'biggest',
}
// cluster tile extent
export const CLUSTER_EXTENT = 512

/*
 * Action types
 */

// Related to badus api
export const BADUS_FETCH = 'badus/fetch'
export const BADUS_FETCH_SUCCESS = 'badus/fetch::success'
export const BADUS_FETCH_ERROR = 'badus/fetch::error'
export const BADUS_POST = 'badus/post'
export const BADUS_POST_SUCCESS = 'badus/post::success'
export const BADUS_POST_ERROR = 'badus/post::error'
export const BADUS_VALIDATION_ERROR = 'badus/validation::error'

// Not directly related to badus api
export const BADUS_FILTERS_CLEAR = 'badus/filter::clear'
export const BADUS_FILTERS_OPEN = 'badus/filter::open'
export const BADUS_FILTERS_CLOSED = 'badus/filter::closed'
export const BADUS_FILTERS_ALL = 'badus/filter::all'
export const BADUS_TOGGLE_OPEN = 'badus/toggle::open'
export const BADUS_TOGGLE_CLOSED = 'badus/toggle::closed'

// Related to map actions
export const MAP_SET_CENTER = 'map/center::set'
export const MAP_SET_CURRENT_CENTER = 'map/center::setCurrent'
export const MAP_ZOOM_SET = 'map/zoom::set'
export const MAP_ZOOM_INCREASE = 'map/zoom::increase'
export const MAP_ZOOM_DECREASE = 'map/zoom::decrease'
export const MAP_POPUP_OPEN = 'map/popup::open'
export const MAP_POPUP_CLOSE = 'map/popup::close'

// Related to badus form action
export const FORM_OPEN = 'form/open'
export const FORM_CLOSE = 'form/close'
export const FORM_FIELD_UPDATE = 'form/fields::update'
export const FORM_LOCATION_SOURCE_SET = 'form/locationSource::set'

// Related to geolocation
export const GEO_LOCATION = 'geolocation/init'
export const GEO_LOCATION_SUCCESS = 'geolocation/init::success'
export const GEO_LOCATION_ERROR = 'geolocation/init:error'

export const GEO_LOCATION_WATCH = 'geolocation/watch'
export const GEO_LOCATION_WATCH_SUCCESS = 'geolocation/watch::success'
export const GEO_LOCATION_WATCH_ERROR = 'geolocation/watch::error'

export const GEO_QUERY = 'geocode/query'
export const GEO_QUERY_SUCCESS = 'geocode/query::success'
export const GEO_QUERY_ERROR = 'geocode/query::error'
