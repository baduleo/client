import MockDate from 'mockdate'

import { now, format, isOpen, toMinutes } from './time'

afterEach(() => {
  MockDate.reset()
})

describe('utils::time', () => {
  describe('::now', () => {
    it('returns the current time in hh:ss format', () => {
      const time = new Date()
      expect(now(time)).toEqual(`${time.getHours()}:${time.getMinutes()}`)
    })

    it('sets the current time by default', () => {
      MockDate.set('2023-06-16 23:32')
      expect(now()).toBe('23:32')
    })
  })
  describe('::format', () => {
    it('should return a time in hh:mm format from hh:mm:ss', () => {
      expect(format('23:32:12')).toBe('23:32')
    })
    it('should return a time in hh:mm format from hh:mm', () => {
      expect(format('23:32')).toBe('23:32')
    })
  })
  describe('::toMinutes', () => {
    it('should convert any hh:mm time to minutes', () => {
      expect(toMinutes('01:00')).toBe(60)
      expect(toMinutes('22:23')).toBe(1343)

      // It should even work with invalid times
      expect(toMinutes('32:99')).toBe(2019)
    })

    it('should remove seconds (and work) from any hh:mm:ss time format', () => {
      expect(toMinutes('01:00:23')).toBe(60)
      expect(toMinutes('22:23:45')).toBe(1343)
    })
  })
  describe('::isOpen', () => {
    it('should return true when the specified time is in the two hours range', () => {
      // They should be open exactly at the time
      expect(isOpen('09:00', '23:00', '09:00')).toBe(true)
      expect(isOpen('09:00', '23:00', '23:00')).toBe(true)

      // Or when time is inside the range
      expect(isOpen('09:00', '22:00', '13:45')).toBe(true)
      expect(isOpen('22:00', '03:00', '23:32')).toBe(true)
      expect(isOpen('00:00', '06:00', '02:43')).toBe(true)
      expect(isOpen('01:00', '00:00', '23:00')).toBe(true)
      // TODO: these two tests don't pass currently
      // expect(isOpen('08:00', '02:00', '00:00')).toBe(true)
      // expect(isOpen('08:00', '02:00', '00:05')).toBe(true)
      // Even when it's open 24h!
      expect(isOpen('00:00', '00:00', '23:00')).toBe(true)
    })

    it('should return false when the specified time is outside the range', () => {
      expect(isOpen('09:00', '22:00', '08:59')).toBe(false)
      expect(isOpen('09:00', '22:00', '22:01')).toBe(false)

      expect(isOpen('09:00', '22:00', '02:30')).toBe(false)
      expect(isOpen('22:00', '03:00', '04:20')).toBe(false)
    })
  })
})
