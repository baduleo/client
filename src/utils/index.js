/**
 * Processes a response so, if it's valid, returns the response
 * data object. Otherwise throws the response as an error, so it
 * can be catched with `catch`.
 *
 * @param {object} response The response object.
 * @returns {Promise}
 */
export const processResponse = (response) => new Promise((resolve, reject) => {
  if (response.status < 400) {
    return resolve(response.data)
  }

  return reject(response)
})

/**
 * Ensures a string has its first
 * character as uppercase.
 *
 * @param {string} string String to be converted.
 */
export const ucfirst = (string) =>
  string.substr(0, 1).toUpperCase() + string.substr(1)

/**
 * Returns an error (if available) from the errors
 * object returned by the API.
 *
 * @param {object} errors Object with form errors.
 * @param {string} name The field name.
 */
export const getError = (errors, name) => {
  if (!errors || !errors[name]) {
    return null
  }

  return errors[name].message
}
