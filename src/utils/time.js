/**
 * Returns the current time in hh:mm format.
 *
 * @return {string}
 */
export const now = (time = new Date()) =>
  `${time.getHours()}:${time.getMinutes()}`

export const parse = (timestamp) => timestamp.split(':')

/**
 * Always returns a time in hh:mm format.
 *
 * @param {string} timestamp The timestamp in hh:mm(:ss)? format
 */
export const format = (timestamp) => {
  const [hour, minutes] = parse(timestamp)

  return `${hour}:${minutes}`
}

export const toMinutes = (timestamp) => {
  const [hours, minutes] = parse(timestamp)

  return parseInt(hours, 10) * 60 + parseInt(minutes, 10)
}

/**
 * Determines if in the specified ${check} time
 * it should be open based on ${from} and ${to}
 * times.
 *
 * @param {string} from From opening time.
 * @param {string} to To closing time.
 * @param {string} check Check this time.
 */
export const isOpen = (from, to, check = now()) => {
  from = toMinutes(from)
  to = toMinutes(to)
  check = toMinutes(check)

  if (from < to) {
    return check >= from && check <= to
  }

  return check >= from && check >= to
}
