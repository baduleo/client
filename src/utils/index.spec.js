import { processResponse, ucfirst } from '.'

describe('utils', () => {
  describe('::processResponse', () => {
    const response = {
      status: 200,
      data: ['testing'],
    }
    it('returns the response data object if response is valid', async() => {
      await expect(processResponse(response)).resolves.toEqual(response.data)
    })
    it('throws the entire response object if is not valid', async() => {
      const errored = {
        ...response,
        status: 403,
      }
      await expect(processResponse(errored)).rejects.toBe(errored)
    })
  })
  it('::ucfirst', () => {
    expect(ucfirst('testing')).toEqual('Testing')
    expect(ucfirst('àustria')).toEqual('Àustria')
  })
})
