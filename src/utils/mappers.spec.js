import { ids } from './mappers'

describe('mappers', () => {
  describe('::idsMapper', () => {
    it('should properly map array of objects by their keys', () => {
      const array = [
        {
          id: 'first',
          name: 'John Doe',
        },
        {
          id: 'second',
          name: 'Fulanito',
        },
      ]

      const mapped = ids(array)

      expect(mapped.all).toBeDefined()
      expect(mapped.ids).toBeDefined()
      expect(typeof mapped.all).toBe('object')
      expect(Array.isArray(mapped.ids)).toBeTruthy()

      expect(mapped.ids).toEqual(['first', 'second'])
      expect(mapped.all).toEqual({
        first: {
          id: 'first',
          name: 'John Doe',
        },
        second: {
          id: 'second',
          name: 'Fulanito',
        },
      })
    })

    it('should properly map array of objects by the specified key', () => {
      const array = [
        {
          _id: 'first',
          name: 'John Doe',
        },
        {
          _id: 'second',
          name: 'Fulanito',
        },
      ]

      const mapped = ids(array, '_id')

      expect(mapped.all).toBeDefined()
      expect(mapped.ids).toBeDefined()
      expect(typeof mapped.all).toBe('object')
      expect(Array.isArray(mapped.ids)).toBeTruthy()

      expect(mapped.ids).toEqual(['first', 'second'])
      expect(mapped.all).toEqual({
        first: {
          _id: 'first',
          name: 'John Doe',
        },
        second: {
          _id: 'second',
          name: 'Fulanito',
        },
      })
    })
  })
})
