import React, { Component } from 'react'
import { Route, Switch } from 'react-router'

import Main from '../../containers/Main'
import NotFound from '../Pages/NotFound'

class Routes extends Component {
  render() {
    return (
      <Switch>
        <Route exact path='/' component={Main} />
        <Route component={NotFound} />
      </Switch>
    )
  }
}

export default Routes
