import React from 'react'
import { MemoryRouter } from 'react-router'
import { mount } from 'enzyme'

import NotFound from '../Pages/NotFound'

import Routes from '.'

import 'mapbox-gl/dist/mapbox-gl.css'

describe('<Routes />', () => {
  // describe('/', () => {
  //   it('renders Main component', () => {
  //     const routes = mount(
  //       <Provider store={store}>
  //         <MemoryRouter>
  //           <Routes />
  //         </MemoryRouter>
  //       </Provider>
  //     )

  //     expect(routes.find(Main).length).toEqual(1)
  //   })
  // })

  describe('/404-not-found', () => {
    it('renders the NotFound component on unknown pages', () => {
      const routes = mount(
        <MemoryRouter initialEntries={['/404']}>
          <Routes />
        </MemoryRouter>
      )

      expect(routes.find(NotFound).length).toEqual(1)
    })
  })
})
