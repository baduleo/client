import { Component } from 'react'
import PropTypes from 'prop-types'

class Geolocation extends Component {
  static propTypes = {
    watch: PropTypes.bool,
    timeout: PropTypes.number,
    maximumAge: PropTypes.number,
    enableHighAccuracy: PropTypes.bool,
    init: PropTypes.func.isRequired,
    success: PropTypes.func.isRequired,
    error: PropTypes.func.isRequired,
  }

  static defaultProps = {
    watch: false,
    // geoOptions defaults to a cached position
    // if it fails it tries to get a real position
    timeout: 0,
    maximumAge: Infinity,
    enableHighAccuracy: false,
  }

  geoLocationEnabled = 'geolocation' in navigator
  localStorage = 'localStorage' in window

  get geoOptions() {
    const options = {
      enableHighAccuracy: this.props.enableHighAccuracy,
      maximumAge: this.props.maximumAge,
      timeout: this.props.timeout,
    }

    const validation = {
      enableHighAccuracy: (prop) => typeof prop === 'boolean',
      maximumAge: (prop) => typeof prop === 'number',
      timeout: (prop) => typeof prop === 'number',
    }

    // only pass values with valid type
    return Object
      .keys(options)
      .reduce((acc, key) => {
        const option = options[key]
        const validationFn = validation[key]
        return validationFn(option)
          ? { ...acc, [key]: option }
          : { ...acc }
      }, {})
  }

  saveLocation = position => {
    const {
      latitude,
      longitude,
      altitude,
      accuracy,
      altitudeAccuracy,
      heading,
      speed,
    } = position.coords

    const options = {
      latitude,
      longitude,
      altitude,
      accuracy,
      altitudeAccuracy,
      heading,
      speed,
    }

    this.savePositionToLocalStorage(options)
    this.props.success(options)
  }

  // get cached location
  handleGeoLocationError = error => {
    switch (error.code) {
      // permission expired
      case 3: {
        // Quick fallback when no suitable cached position exists.
        navigator
          .geolocation
          .getCurrentPosition(
            this.saveLocation,
            (err) => err.code === 1 ? this.handleGeoLocationError(err) : this.props.error(err)
          )

        break
      }

      default : {
        this.props.error(error)
      }
    }
  }

  getLocation = () => {
    if (!this.geoLocationEnabled) return

    navigator
      .geolocation
      .getCurrentPosition(
        this.saveLocation,
        this.handleGeoLocationError,
        this.geoOptions
      )
  }

  watch = () => {
    if (!this.geoLocationEnabled) return

    navigator
      .geolocation
      .watchPosition(
        this.saveLocation,
        this.handleGeoLocationError,
        this.geoOptions
      )
  }

  savePositionToLocalStorage = (options = {}) => {
    if (!this.localStorage) return

    localStorage.setItem('react-redux-geo-position', JSON.stringify(options))
  }

  getPositionFromLocalStorage = () => {
    if (!this.localStorage) return

    let position = localStorage.getItem('react-redux-geo-position')

    if (!position || typeof position !== 'string') {
      return
    }

    try {
      position = JSON.parse(position)
      this.props.success(position)
    } catch (err) {
      console.error('invalid position object in localStorage')
    }
  }

  componentDidMount() {
    this.props.init()

    this.getPositionFromLocalStorage()

    this.props.watch
      ? this.watch()
      : this.getLocation()
  }

  render() {
    return null
  }
}

export default Geolocation
