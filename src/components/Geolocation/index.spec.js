import React from 'react'
import { shallow, mount } from 'enzyme'

import Geolocation from '.'

const props = {
  init: jest.fn(),
  success: jest.fn(),
  error: jest.fn(),
}

describe('<Geolocation />', () => {
  describe('::render', () => {
    it('renders as expected', () => {
      const wrapper = mount(
        <Geolocation
          {...props}
        />
      )

      expect(wrapper.find(Geolocation).length).toBe(1)
    })

    it('renders nothing', () => {
      const wrapper = mount(
        <Geolocation
          {...props}
        />
      )

      expect(wrapper.children().length).toBe(0)
    })
  })

  describe('::geoOptions', () => {
    it('should clean empty geoOptions', () => {
      const wrapper = shallow(
        <Geolocation
          timeout={null}
          maximumAge={null}
          enableHighAccuracy={null}
          {...props}
        />
      )

      const instance = wrapper.instance()

      const options = instance.geoOptions
      expect(options).toEqual({})
    })
    it('should accept geoOptions and return an object with 3 props', () => {
      const wrapper = shallow(
        <Geolocation
          timeout={23}
          maximumAge={32}
          enableHighAccuracy={true}
          {...props}
        />
      )

      const instance = wrapper.instance()

      const options = instance.geoOptions
      expect(options).toEqual({
        enableHighAccuracy: true,
        timeout: 23,
        maximumAge: 32,
      })
    })
  })

  describe('::watch', () => {
    it('should call watch method when enabled', () => {
      const wrapper = shallow(
        <Geolocation
          watch
          {...props}
        />
      )

      const instance = wrapper.instance()
      const spy = jest.spyOn(instance, 'watch')
      instance.componentDidMount()

      expect(spy).toHaveBeenCalled()
      expect(spy.mock.calls.length).toBe(1)
    })
  })

  describe('::getLocation', () => {
    it('should call the getLocation fn', () => {
      const wrapper = shallow(
        <Geolocation {...props} />
      )

      const instance = wrapper.instance()
      const spy = jest.spyOn(instance, 'getLocation')
      instance.componentDidMount()

      expect(spy).toHaveBeenCalled()
      expect(spy.mock.calls.length).toBe(1)
    })
  })
})
