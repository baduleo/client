import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons'

import { query } from '../../actions/geolocation'
import Button from '../Button'
import FormGroup from './FormGroup'
import Input from './Input'

import './Address.scss'

class Address extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    form: PropTypes.shape({
      form: PropTypes.shape({
        lat: PropTypes.number,
        lng: PropTypes.number,
        address: PropTypes.string,
      }).isRequired,
    }).isRequired,
  }

  onUpdateAddressClick() {
    const {
      dispatch,
      form: {
        form: { lat, lng },
      },
    } = this.props

    dispatch(query(`${lng},${lat}`))
  }

  onFocusAddress() {
    const {
      dispatch,
      form: {
        form: { address, lat, lng },
      },
    } = this.props

    if (!address || (address && !address.length)) {
      dispatch(query(`${lng},${lat}`))
    }
  }

  render() {
    const { name, label, form: { loading } } = this.props

    return (
      <FormGroup name={name} label={label}>
        <div className='address-wrapper'>
          <Input {...this.props} onFocus={this.onFocusAddress.bind(this)} />
          <Button
            className='address-update'
            title='Tomar de la ubicación'
            active={loading}
            onClick={this.onUpdateAddressClick.bind(this)}
          >
            <FontAwesomeIcon icon={faMapMarkerAlt} />
          </Button>
        </div>
      </FormGroup>
    )
  }
}

export default Address
