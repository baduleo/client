import React from 'react'
import { shallow } from 'enzyme'

import FormGroup from './FormGroup'

const props = {
  dispatch: jest.fn(),
  name: 'test',
}

describe('<Form/FormGroup />', () => {
  describe('::render', () => {
    it('renders as expected', () => {
      const component = shallow(<FormGroup {...props} />)

      expect(component.find('div.form-group').length).toBe(1)
    })
    it('sets a label with the name, if none is set', () => {
      const component = shallow(<FormGroup {...props} />)

      expect(component.find('label').text()).toBe('Test')
    })
    it('appends a className if specified', () => {
      const component = shallow(<FormGroup {...props} className='testing' />)

      expect(component.find('.form-group').prop('className')).toBe('form-group testing')
    })
  })
})
