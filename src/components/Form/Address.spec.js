import React from 'react'
import { shallow } from 'enzyme'

import { defaultFormState } from '../../reducers/form'

import Button from '../Button'
import FormGroup from './FormGroup'
import Address from './Address'
import Input from './Input'

const props = {
  dispatch: jest.fn(),
  form: {
    ...defaultFormState,
    active: true,
    form: {
      lat: 41,
      lng: 1.23,
    },
  },
  name: 'address',
  label: 'Address',
}

describe('<Form/Address />', () => {
  describe('::render', () => {
    it('renders as expected', () => {
      const component = shallow(<Address {...props} />)

      expect(component.find(FormGroup).length).toBe(1)
      expect(component.find(Input).length).toBe(1)
      expect(component.find(Button).length).toBe(1)
    })
  })
  describe('::onUpdateAddressClick', () => {
    it('calls dispatch(query) on click the locate button', () => {
      const spied = {
        ...props,
        dispatch: jest.fn(),
      }

      const component = shallow(<Address {...spied} />)

      component.find(Button).simulate('click', {
        preventDefault: jest.fn(),
      })
      expect(spied.dispatch).toHaveBeenCalled()
      expect(typeof spied.dispatch.mock.calls[0][0]).toEqual('function')
    })
  })
  describe('::onFocusAddress', () => {
    it('calls dispatch if there\'s no address', () => {
      const spied = {
        ...props,
        dispatch: jest.fn(),
      }

      const component = shallow(<Address {...spied} />)

      component.find(Input).simulate('focus')
      expect(spied.dispatch).toHaveBeenCalled()
      expect(typeof spied.dispatch.mock.calls[0][0]).toEqual('function')
    })
    it('does not call dispatch if there\'s an address', () => {
      const spied = {
        ...props,
        dispatch: jest.fn(),
        form: {
          form: {
            ...props.form.form,
            address: 'Something',
          },
        },
      }

      const component = shallow(<Address {...spied} />)

      component.find(Input).simulate('focus')
      expect(spied.dispatch).not.toHaveBeenCalled()
    })
  })
})
