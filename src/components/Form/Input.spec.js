import React from 'react'
import { shallow } from 'enzyme'

import { update } from '../../actions/form'
import Input from './Input'

const props = {
  dispatch: jest.fn(),
  form: {
    form: {},
  },
  name: 'test',
}

describe('<Form/Input />', () => {
  describe('::render', () => {
    it('renders as expected', () => {
      const component = shallow(<Input {...props} />)

      expect(component.find('input').length).toBe(1)
    })
    it('sets the proper type by default', () => {
      const component = shallow(<Input {...props} />)

      expect(component.find('input').prop('type')).toBe('text')
    })
    it('a specified type to the input', () => {
      const component = shallow(<Input {...props} type='number' />)

      expect(component.find('input').prop('type')).toBe('number')
    })
    it('passes any other valid DOM prop to the element', () => {
      const component = shallow(<Input {...props} onClick={() => true} />)

      expect(component.find('input').prop('onClick')).toBeDefined()
      expect(typeof component.find('input').prop('onClick')).toBe('function')
    })
    it('appends a className if specified', () => {
      const component = shallow(<Input {...props} className='testing' />)

      expect(component.find('input').prop('className')).toBe('form-control testing')
    })
    it('generates a textarea if type === textarea', () => {
      const component = shallow(<Input {...props} type='textarea' />)

      expect(component.find('textarea').length).toBe(1)
    })
    it('adds the is-invalid class in case the field has errors', () => {
      const properties = {
        ...props,
        form: {
          ...props.form,
          errors: {
            test: {
              message: 'error!',
            },
          },
        },
      }
      const component = shallow(<Input {...properties} />)

      expect(component.find('input').prop('className')).toMatch('is-invalid')
    })
  })

  describe('::componentDidMount', () => {
    it('sets a defaultValue if specified', async() => {
      const spied = {
        ...props,
        dispatch: jest.fn(),
        defaultValue: 23,
      }
      const component = shallow(<Input {...spied} />)

      expect(component.find('input').prop('value')).toEqual(23)
    })
    it('dispatches update() when defaultValue is set', async() => {
      const spied = {
        ...props,
        dispatch: jest.fn(),
        defaultValue: 23,
      }
      shallow(<Input {...spied} />)

      expect(spied.dispatch).toHaveBeenCalled()
      expect(spied.dispatch.mock.calls[0][0]).toEqual(update('test', 23))
    })
    it('does not dispatch update if defaultValue && form.form[name] are set', () => {
      const spied = {
        ...props,
        dispatch: jest.fn(),
        defaultValue: 23,
        form: {
          form: {
            test: 'testing',
          },
        },
      }
      const component = shallow(<Input {...spied} />)

      expect(spied.dispatch).not.toHaveBeenCalled()
      expect(component.find('input').prop('value')).toEqual('testing')
    })
  })

  describe('::onChange', () => {
    it('calls dispatch onChange event', () => {
      const spied = {...props, dispatch: jest.fn()}
      const component = shallow(<Input {...spied} />)

      component.find('input').simulate('change', {target: {value: 23}})

      expect(spied.dispatch).toHaveBeenCalled()
    })
    it('calls update() on dispatch, with the proper values', () => {
      const spied = {...props, dispatch: jest.fn()}
      const component = shallow(<Input {...spied} />)

      component.find('input').simulate('change', {target: {value: 23}})

      expect(spied.dispatch.mock.calls[0][0]).toEqual(update('test', 23))
    })
  })
})
