import React from 'react'
import { shallow } from 'enzyme'

import Button from '../Button'
import Location from './Location'
import Input from './Input'

const props = {
  dispatch: jest.fn(),
  name: 'test',
  geolocation: {
    watch: {
      latitude: 23,
      longitude: 32,
    },
  },
  form: {
    locationSource: 'watch',
    form: {

    },
  },
  map: {
    center: [32, 23],
  },
}

describe('<Form/Location />', () => {
  describe('::render', () => {
    it('renders as expected', () => {
      const component = shallow(<Location {...props} />)

      expect(component.find('div.input-location').length).toBe(1)
      expect(component.find('small').length).toBe(1)
    })
    it('renders two buttons inside input-group-prepend', () => {
      const component = shallow(<Location {...props} />)
      const prepend = component.find('div.input-group-prepend')

      expect(prepend.length).toBe(1)
      expect(prepend.find(Button).length).toBe(2)
    })
    it('renders two inputs inside', () => {
      const component = shallow(<Location {...props} />)

      expect(component.find(Input).length).toBe(2)
    })
    it('renders a small note at the bottom', () => {
      const component = shallow(<Location {...props} />)

      expect(component.find('small').length).toBe(1)
    })
    it('changes the small note at the bottom on different locationSource', () => {
      const properties = {
        ...props,
        form: {
          ...props.form,
          locationSource: 'map',
        },
      }
      const component = shallow(<Location {...properties} />)

      expect(component.find('small').text()).toMatch(/mapa$/)
    })
  })
  describe('::mapSource', () => {
    it('calls setLocationSource on btn click', () => {
      const spied = {...props, dispatch: jest.fn()}
      const component = shallow(<Location {...spied} />)

      component.find('#geo-source-map').simulate('click')

      expect(spied.dispatch).toHaveBeenCalled()
      expect(typeof spied.dispatch.mock.calls[0][0]).toBe('function')
    })
  })
  describe('::watchSource', () => {
    it('calls setLocationSource on btn click', () => {
      const spied = {...props, dispatch: jest.fn()}
      const component = shallow(<Location {...spied} />)

      component.find('#geo-source-watch').simulate('click')

      expect(spied.dispatch).toHaveBeenCalled()
      expect(typeof spied.dispatch.mock.calls[0][0]).toBe('function')
    })
  })
})
