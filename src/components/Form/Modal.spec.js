import React from 'react'
import { shallow } from 'enzyme'

import { defaultBadusState } from '../../reducers/badus'
import { defaultFormState } from '../../reducers/form'
import { close } from '../../actions/form'

import Submit from '../Button/Submit'
import Modal from './Modal'
import Form from '.'

const props = {
  dispatch: jest.fn(),
  badus: defaultBadusState,
  form: {
    ...defaultFormState,
    active: true,
  },
  geolocation: {
    watch: {
      latitude: 23,
      longitude: 23,
    },
  },
}

describe('<Form/Modal />', () => {
  describe('::render', () => {
    it('renders as expected', () => {
      const component = shallow(<Modal {...props} />)

      expect(component.find('div.form-modal').length).toBe(1)
    })
    it('renders the inner form', () => {
      const component = shallow(<Modal {...props} />)
      expect(component.find(Form).length).toBe(1)
    })
    it('does not render if !form.active', () => {
      const altered = {
        ...props,
        form: {
          ...props.form,
          active: false,
        },
      }
      const component = shallow(<Modal {...altered} />)

      expect(component.find('div.form-modal').length).toBe(0)
      expect(component.html()).toBeFalsy()
    })
    it('renders the submit button', () => {
      const component = shallow(<Modal {...props} />)
      expect(component.find(Submit).length).toBe(1)
    })
  })
  describe('::close', () => {
    it('renders the close button', () => {
      const component = shallow(<Modal {...props} />)

      expect(component.find('div.modal-header button.close').length).toBe(1)
    })
    it('calls dispatch(close) on click the close button', () => {
      const spied = {
        ...props,
        dispatch: jest.fn(),
      }

      const component = shallow(<Modal {...spied} />)

      component.find('button.close').simulate('click')
      expect(spied.dispatch).toHaveBeenCalled()
      expect(spied.dispatch.mock.calls[0][0]).toEqual(close())
    })
  })
})
