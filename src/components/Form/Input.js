import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import cleanProps from 'clean-react-props'
import classNames from 'classnames'

import { update } from '../../actions/form'
import { getError } from '../../utils'
import Error from './Error'

class Input extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    form: PropTypes.object.isRequired,
    name: PropTypes.string.isRequired,
    type: PropTypes.string,
  }

  onChange({target: { value }}) {
    const { dispatch, name } = this.props

    return dispatch(update(name, value))
  }

  componentDidMount() {
    const { name, defaultValue, dispatch, form: { form } } = this.props

    if (defaultValue && !form[name]) {
      dispatch(update(name, defaultValue))
    }
  }

  render() {
    const { name, defaultValue, value, form: { form, errors }} = this.props
    const props = {...this.props}

    if (!props.type) {
      props.type = 'text'
    }

    const className = ['form-control']
    if (props.className) {
      className.push(props.className)
    }

    if (getError(errors, name)) {
      className.push('is-invalid')
    }

    let Tag = 'input'
    if (props.type === 'textarea') {
      Tag = 'textarea'
      delete props.type
    }

    return (
      <Fragment>
        <Tag
          {...cleanProps(props)}
          className={classNames(className)}
          onChange={this.onChange.bind(this)}
          value={form[name] || value || defaultValue || ''}
        />
        <Error name={name} errors={errors} />
      </Fragment>
    )
  }
}

export default Input
