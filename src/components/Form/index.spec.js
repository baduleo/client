import React from 'react'
import { shallow } from 'enzyme'

import Form from '.'

const props = {
  dispatch: jest.fn(),
  geolocation: {
    watch: {
      latitude: 23,
      longitude: 32,
    },
  },
  form: {
    form: {},
  },
}

describe('<Form/Form />', () => {
  describe('::render', () => {
    it('renders as expected', () => {
      const component = shallow(<Form {...props} />)
      expect(component.length).toBe(1)
      expect(component.find('form').length).toBe(1)
    })
  })
})
