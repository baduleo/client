import React, { Component } from 'react'
import propTypes from 'prop-types'

import Field from './Field'
import Location from './Location'
import Address from './Address'

import './index.scss'

class Form extends Component {
  static propTypes = {
    form: propTypes.object.isRequired,
  }

  render() {

    return (
      <form>
        <Field
          name='name'
          label='Nombre:'
          {...this.props}
        />
        <Field
          name='from'
          label='Abierto desde:'
          type='time'
          defaultValue='07:00'
          {...this.props}
        />
        <Field
          name='to'
          label='Abierto hasta:'
          type='time'
          defaultValue='23:30'
          {...this.props}
        />
        <Location {...this.props} />
        <Address
          name='address'
          label='Dirección:'
          type='textarea'
          {...this.props}
        />
      </form>
    )
  }
}

export default Form
