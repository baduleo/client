import React from 'react'
import { shallow } from 'enzyme'

import Field from './Field'
import FormGroup from './FormGroup'
import Input from './Input'

const props = {
  dispatch: jest.fn(),
  name: 'test',
  label: 'Test',
  form: {
    form: {},
  },
}

describe('<Form/Field />', () => {
  describe('::render', () => {
    it('renders as expected', () => {
      const component = shallow(<Field {...props} />)

      expect(component.find(FormGroup).length).toBe(1)
      expect(component.find(Input).length).toBe(1)
    })
  })
})
