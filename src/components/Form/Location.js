import React, { Component } from 'react'
import propTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCompass, faMap } from '@fortawesome/free-solid-svg-icons'

import { setLocationSource } from '../../actions/form'
import Button from '../Button'
import Input from './Input'


class Location extends Component {
  static propTypes = {
    geolocation: propTypes.shape({
      watch: propTypes.shape({
        latitude: propTypes.number.isRequired,
        longitude: propTypes.number.isRequired,
      }).isRequired,
    }).isRequired,
  }

  render() {
    const {
      form: { locationSource, form },
    } = this.props

    const { lat, lng } = form
    let note = 'Los datos son tomados de tu ubicación actual'

    if (locationSource === 'map') {
      note = 'Los datos son tomados del centro actual del mapa'
    }

    return (
      <div className='input-location'>
        <label>Geolocalicación:</label>
        <div className='input-group'>
          <div className='input-group-prepend'>
            <Button
              id='geo-source-watch'
              active={locationSource === 'watch'}
              onClick={() => this.props.dispatch(setLocationSource('watch'))}
            >
              <FontAwesomeIcon icon={faCompass} />
            </Button>
            <Button
              id='geo-source-map'
              active={locationSource === 'map'}
              onClick={() => this.props.dispatch(setLocationSource('map'))}
            >
              <FontAwesomeIcon icon={faMap} />
            </Button>
          </div>
          <Input
            name='lat'
            title='Latitud'
            type='number'
            value={lat}
            readOnly
            {...this.props}
          />
          <Input
            name='lng'
            title='Longitud'
            type='number'
            value={lng}
            readOnly
            {...this.props}
          />
        </div>
        <small>{note}</small>
      </div>
    )
  }
}

export default Location
