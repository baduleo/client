import React, { Component } from 'react'
import { childrenOfType } from 'airbnb-prop-types'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import { ucfirst } from '../../utils'
import Input from './Input'

class FormGroup extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    children: PropTypes.oneOfType([
      childrenOfType(Input),
      childrenOfType('div'),
    ]),
    label: PropTypes.string,
  }

  render() {
    const { children, className, name } = this.props
    let { label } = this.props

    const classes = ['form-group']
    if (className) {
      classes.push(className)
    }
    if (!label) {
      label = ucfirst(name)
    }

    return (
      <div className={classNames(classes)}>
        <label htmlFor={name}>{label}</label>
        {children}
      </div>
    )
  }
}

export default FormGroup
