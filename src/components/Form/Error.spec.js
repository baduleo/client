import React from 'react'
import { shallow } from 'enzyme'

import Error from './Error'

const props = {
  dispatch: jest.fn(),
  name: 'test',
  errors: {
    test: {
      message: 'error!',
    },
  },
}

describe('<Form/Error />', () => {
  describe('::render', () => {
    it('does not render if there\'s no error', () => {
      const component = shallow(<Error {...props} name='whatever' />)

      expect(component.html()).toBeFalsy()
    })
    it('renders as expected', () => {
      const component = shallow(<Error {...props} />)

      expect(component.html())
        .toBe('<div class="invalid-feedback">error!</div>')
    })
    it('replaces ` characters with code tags', () => {
      const properties = {
        ...props,
        errors: {
          test: {
            message: '`error`!',
          },
        },
      }
      const component = shallow(<Error {...properties} />)

      expect(component.html())
        .toBe('<div class="invalid-feedback"><code>error</code>!</div>')
    })
  })
})
