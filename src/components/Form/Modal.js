import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { close } from '../../actions/form'

import Submit from '../Button/Submit'
import Form from '.'

import './Modal.scss'

class Modal extends Component {
  static propTypes = {
    form: PropTypes.shape({
      active: PropTypes.bool.isRequired,
    }).isRequired,
  }

  render() {
    const {
      form: { active },
    } = this.props

    if (!active) {
      return null
    }

    return (
      <div className='form-modal' tabIndex={-1} role='dialog'>
        <div className='modal-dialog' role='document'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h5 className='modal-title'>Añadir badu</h5>
              <button
                type='button'
                className='close'
                data-dismiss='modal'
                aria-label='Close'
                onClick={() => this.props.dispatch(close())}
              >
                <span aria-hidden>&times;</span>
              </button>
            </div>
            <div className='modal-body'>
              <Form {...this.props} />
            </div>
            <div className='modal-footer'>
              <Submit {...this.props} />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Modal
