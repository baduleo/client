import React, { Component } from 'react'
import PropTypes from 'prop-types'

import FormGroup from './FormGroup'
import Input from './Input'

class Field extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
  }

  render() {
    const { name, label } = this.props

    return (
      <FormGroup name={name} label={label}>
        <Input {...this.props} />
      </FormGroup>
    )
  }
}

export default Field
