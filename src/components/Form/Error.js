import React, { Component } from 'react'
import { getError } from '../../utils'

import './Error.scss'

class Error extends Component {
  render() {
    const { name, errors } = this.props

    if (!errors[name]) {
      return null
    }

    let error = getError(errors, name)
    if (error.match(/`(\w+)`/)) {
      error = error.replace(/`(\w+)`/, '<code>$1</code>')
    }

    return (
      <div
        className='invalid-feedback'
        dangerouslySetInnerHTML={{__html: error}}
      />
    )
  }
}

export default Error
