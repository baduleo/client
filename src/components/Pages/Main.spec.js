import React from 'react'
import { shallow } from 'enzyme'

import { defaultMapState } from '../../reducers/map'
import { defaultBadusState } from '../../reducers/badus'

import Map from '../Map'
import Main from './Main'

const props = {
  dispatch: jest.fn(),
  map: defaultMapState,
  badus: defaultBadusState,
}

describe('<Pages/Map />', () => {
  describe('::render', () => {
    it('renders without crashing', () => {
      const page = shallow(<Main {...props} />)

      expect(page.find(Map).length).toEqual(1)
    })
  })
})
