import React, { Component, Fragment } from 'react'

import { fetch } from '../../actions/badus'

import Locate from '../../containers/Geolocation/Locate'
import Watch from '../../containers/Geolocation/Watch'
import Map from '../Map'

import './Main.scss'

class Main extends Component {
  componentDidMount() {
    this.props.dispatch(fetch())
  }

  render() {
    return (
      <Fragment>
        <Locate enableHighAccuracy />
        <Watch watch enableHighAccuracy />
        <Map {...this.props} />
      </Fragment>
    )
  }
}

export default Main
