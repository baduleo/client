import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'

class NotFound extends Component {
  static propTypes = {
    location: PropTypes.object.isRequired,
  }

  render() {
    const { location: { pathname } } = this.props

    return (
      <Fragment>
        <h1>404</h1>
        <p>The page <em>{pathname}</em> could not be found</p>
      </Fragment>
    )
  }
}

export default NotFound
