import React from 'react'
import { shallow } from 'enzyme'
import NotFound from './NotFound'

const props = {
  location: {
    pathname: '/404',
  },
}

describe('<Pages/NotFound />', () => {
  describe('::render', () => {
    it('renders as expected', () => {
      const page = shallow(<NotFound {...props} />)
      expect(page.find('h1').text()).toBe('404')
    })
  })
})
