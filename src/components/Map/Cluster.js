import React from 'react'
import { Marker } from 'react-mapbox-gl'

import { CLUSTER_SIZES } from '../../constants'
import { setCenter, increaseZoom } from '../../actions/map'
import store from '../../store'

import './Cluster.scss'

/**
 * Action to be run on cluster click. Note that it has
 * been exported for easier testing. Cluster must be a
 * function, not a class or component, so I need to
 * export the methods this way in order to test them.
 *
 * @param {store} store Redux store.
 * @param {object} coordinates The coordinates object.
 */
export const onClusterClick = (store, coordinates) => {
  store.dispatch(setCenter(coordinates))
  store.dispatch(increaseZoom(2))
}

/**
 * Cluster marker element. It ain't a class because
 * the guys after react-mapbox-gl decided to use a
 * simple function instead of a react component for
 * this.
 *
 * @param {object|array} coordinates Coordinates, or props if we're under testing.
 * @param {number|string} amount The amount of points inside this cluster.
 */
const Cluster = (coordinates, amount/* , bound */) => {
  const className = ['cluster']
  Object.keys(CLUSTER_SIZES).forEach((size) => {
    if (amount > size) {
      className.push(CLUSTER_SIZES[size])
    }
  })

  return (
    <Marker
      anchor='center'
      coordinates={coordinates}
      key={coordinates.join(',')}
      onClick={onClusterClick.bind(this, store, coordinates)}
    >
      <span className={className.join(' ')}>
        {amount}
      </span>
    </Marker>
  )
}

export default Cluster
