import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Cluster } from 'react-mapbox-gl'

import { CLUSTER_EXTENT } from '../../constants'
import ClusterMarker from './Cluster'
import Marker from './Marker'

class Markers extends Component {
  static propTypes = {
    badus: PropTypes.shape({
      visible: PropTypes.array.isRequired,
      all: PropTypes.object.isRequired,
    }).isRequired,
  }

  render() {
    const { visible, all } = this.props.badus

    return (
      <Cluster ClusterMarkerFactory={ClusterMarker} extent={CLUSTER_EXTENT}>
        {
          visible.map((id) => {
            const { lng, lat } = all[id]
            return (
              <Marker
                key={id}
                coordinates={[lng, lat]}
                info={all[id]}
                {...this.props}
              />
            )
          })
        }
      </Cluster>
    )
  }
}

export default Markers
