import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { If } from 'babel-plugin-jsx-control-statements'

import {
  closePopup,
  setZoom,
  setCurrentCenter,
} from '../../actions/map'
import Modal from '../Form/Modal'
import Mapbox from './Mapbox'
import Markers from './Markers'
import Popup from './Popup'
import Actions from './Actions'

import './index.scss'

class Map extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    map: PropTypes.shape({
      center: PropTypes.oneOfType([
        PropTypes.array,
        PropTypes.object,
      ]).isRequired,
    }).isRequired,
  }

  render() {
    const { map, dispatch } = this.props

    return (
      <div className='map'>
        <Mapbox
          style='mapbox://styles/mapbox/streets-v10' // eslint-disable-line
          center={map.center}
          zoom={[map.zoom]}
          onClick={() => {
            dispatch(closePopup())
          }}
          onMoveEnd={(map) => {
            dispatch(setCurrentCenter(map.getCenter()))
          }}
          onZoomEnd={(map) => {
            dispatch(setZoom(map.getZoom()))
          }}
        >
          <Markers {...this.props} />
          <If condition={map.popup}>
            <Popup {...map.popup} />
          </If>
        </Mapbox>
        <Actions {...this.props} />
        <Modal {...this.props} />
      </div>
    )
  }
}

export default Map
