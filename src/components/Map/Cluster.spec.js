import createStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { shallow } from 'enzyme'

import { MAP_SET_CENTER, MAP_ZOOM_INCREASE } from '../../constants'
import Cluster, { onClusterClick } from './Cluster'

const props = {
  amount: 10,
  coordinates: [
    5,
    23,
  ],
}

const mockStore = createStore([thunk])

describe('<Map/Cluster />', () => {
  describe('::render', () => {
    it('should render properly', () => {
      const Wrapper = Cluster(props.coordinates, 13)
      const marker = shallow(Wrapper)

      expect(marker.length).toBe(1)
      expect(marker.find('span').prop('className')).toBe('cluster')
      expect(marker.find('span').text()).toBe('13')
    })

    it('should add the "bigger" class when amount > 20', () => {
      const Wrapper = Cluster(props.coordinates, 23)
      const marker = shallow(Wrapper)

      expect(marker.find('span').prop('className')).toBe('cluster bigger')
      expect(marker.find('span').text()).toBe('23')
    })

    it('should add the "biggest" class when amount > 100', () => {
      const Wrapper = Cluster(props.coordinates, 221)
      const marker = shallow(Wrapper)

      expect(marker.find('span').prop('className')).toBe('cluster bigger biggest')
      expect(marker.find('span').text()).toBe('221')
    })
  })

  describe('::onClusterClick', () => {
    it('dispatches proper actions on click', () => {
      const store = mockStore({})
      const coordinates = {
        lat: 23,
        lng: 32,
      }
      const expected = [
        {
          type: MAP_SET_CENTER,
          center: coordinates,
        },
        {
          type: MAP_ZOOM_INCREASE,
          amount: 2,
        },
      ]
      onClusterClick(store, coordinates)

      expect(store.getActions()).toEqual(expected)
    })
  })
})
