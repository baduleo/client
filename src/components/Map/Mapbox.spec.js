import ReactMapboxGl from 'react-mapbox-gl'
import Mapbox from './Mapbox'

describe('<Map/Mapbox />', () => {
  it('returns an instance of ReactMapboxGl', () => {
    expect(Mapbox instanceof ReactMapboxGl)
  })
})
