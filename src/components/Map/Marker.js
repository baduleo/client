import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Marker as MapboxMarker } from 'react-mapbox-gl'
import classNames from 'classnames'

import './Marker.scss'
import { setCenter, openPopup } from '../../actions/map'
import { isOpen } from '../../utils/time'

class Marker extends Component {
  static propTypes = {
    coordinates: PropTypes.array.isRequired,
    dispatch: PropTypes.func.isRequired,
    info: PropTypes.shape({
      from: PropTypes.string.isRequired,
      to: PropTypes.string.isRequired,
    }).isRequired,
  }

  onClick() {
    const { coordinates, dispatch, info } = this.props

    dispatch(setCenter(coordinates))
    dispatch(openPopup(info))
  }

  render() {
    const { coordinates, info } = this.props
    const className = ['marker']
    if (!isOpen(info.from, info.to)) {
      className.push('closed')
    }

    return (
      <MapboxMarker
        anchor='center'
        coordinates={coordinates}
        onClick={this.onClick.bind(this)}
      >
        <span className={classNames(className)}></span>
      </MapboxMarker>
    )
  }
}

export default Marker
