import React from 'react'
import { shallow } from 'enzyme'

import Add from '../Button/Add'
import Filter from '../Button/Filter'
import Actions from './Actions'

const props = {
  dispatch: jest.fn(),
  badus: {
    ids: [],
    visible: [],
  },
  form: {
    active: false,
  },
}

describe('<Map/Actions />', () => {
  describe('::render', () => {
    it('should render properly', () => {
      const wrapper = shallow(<Actions {...props} />)

      expect(wrapper.length).toBe(1)
      expect(wrapper.find(Add).length).toBe(1)
      expect(wrapper.find(Filter).length).toBe(1)
    })
  })
})
