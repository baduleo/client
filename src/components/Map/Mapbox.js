import ReactMapboxGl from 'react-mapbox-gl'

const Mapbox = ReactMapboxGl({
  accessToken: process.env.MAPBOX_TOKEN,
  dragRotate: false,
})

export default Mapbox
