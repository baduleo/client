import React, { Component } from 'react'
import { Popup as MBPopup } from 'react-mapbox-gl'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import './Popup.scss'
import { format, isOpen } from '../../utils/time'

class Popup extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    from: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired,
    address: PropTypes.string.isRequired,
    lat: PropTypes.number.isRequired,
    lng: PropTypes.number.isRequired,
  }

  render() {
    const { id, name, from, to, address, lat, lng } = this.props

    const className = ['badu-popup']
    if (!isOpen(from, to)) {
      className.push('closed')
    }

    return (
      <MBPopup
        key={id}
        coordinates={[lng, lat]}
        className={classNames(className)}
      >
        <h6>
          {name}
        </h6>
        <div className='schedule'>
          Schedule
          <span className='time'>{format(from)}</span>
          <span className='time'>{format(to)}</span>
        </div>
        <div className='address'>
          {address}
        </div>
      </MBPopup>
    )
  }
}

export default Popup
