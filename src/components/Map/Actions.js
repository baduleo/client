import React, { Component } from 'react'

import Filter from '../Button/Filter'
import Add from '../Button/Add'

import './Actions.scss'

class Actions extends Component {
  render() {
    return (
      <div className='map-actions'>
        <Add {...this.props} />
        <Filter {...this.props} />
      </div>
    )
  }
}

export default Actions
