import React from 'react'
import { shallow } from 'enzyme'
import { Marker as MapboxMarker } from 'react-mapbox-gl'
import MockDate from 'mockdate'

import { FALLBACK_LOCATION } from '../../constants'
import Marker from './Marker'

const props = {
  coordinates: FALLBACK_LOCATION,
  dispatch: jest.fn(),
  info: {
    from: '10:00',
    to: '23:32',
  },
}

afterEach(() => {
  MockDate.reset()
})

describe('<Map/Marker />', () => {
  describe('::render', () => {
    it('should render properly', () => {
      const wrapper = shallow(<Marker {...props} />)
      const marker = wrapper.find(MapboxMarker)

      expect(marker.length).toBe(1)
      expect(marker.find('span').prop('className')).toMatch('marker')
    })
    it('should add the closed className when closed', () => {
      MockDate.set('2023-06-16 23:45')
      const wrapper = shallow(<Marker {...props} />)
      const marker = wrapper.find(MapboxMarker)

      expect(marker.length).toBe(1)
      expect(marker.find('span').prop('className')).toBe('marker closed')
    })
    it('should NOT add the closed className when open', () => {
      MockDate.set('2023-06-16 12:45')
      const wrapper = shallow(<Marker {...props} />)
      const marker = wrapper.find(MapboxMarker)

      expect(marker.length).toBe(1)
      expect(marker.find('span').prop('className')).toBe('marker')
    })
  })
})
