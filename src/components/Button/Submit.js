import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { post } from '../../actions/badus'

import Button from '.'

class Submit extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    form: PropTypes.shape({
      loading: PropTypes.bool.isRequired,
      form: PropTypes.object.isRequired,
    }).isRequired,
  }

  onSubmit(event) {
    const { dispatch, form: { form } } = this.props
    event.preventDefault()

    return dispatch(post(form))
  }

  render() {
    const { form: { loading }} = this.props

    return (
      <Button
        type='submit'
        className='btn-block btn-submit'
        onClick={this.onSubmit.bind(this)}
        active={loading}
        disabled={loading}
      >
        Guardar
      </Button>
    )
  }
}

export default Submit
