import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFilter } from '@fortawesome/free-solid-svg-icons'

import { filters } from '../../actions/badus'

import Button from '.'

class Filter extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    badus: PropTypes.shape({
      ids: PropTypes.array.isRequired,
      visible: PropTypes.array.isRequired,
    }).isRequired,
  }

  onClick() {
    this.props.dispatch(filters.toggleClosed())
  }

  render() {
    const { badus: { ids, visible }} = this.props
    const active = ids.length !== visible.length

    return (
      <Button
        title='Filter closed'
        onClick={this.onClick.bind(this)}
        active={active}
      >
        <FontAwesomeIcon icon={faFilter} />
      </Button>
    )
  }
}

export default Filter
