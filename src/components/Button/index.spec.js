import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { shallow } from 'enzyme'

import Button from './'

const props = {
  dispatch: jest.fn(),
  onClick: jest.fn(),
  active: false,
}

describe('<Button />', () => {
  describe('::render', () => {
    it('renders without crashing', () => {
      const button = shallow((
        <Button {...props}>
          <FontAwesomeIcon />
        </Button>
      ))

      expect(button.find('button').length).toEqual(1)
    })
    it('has type=button, to avoid any form issues', () => {
      const button = shallow((
        <Button {...props}>
          <FontAwesomeIcon />
        </Button>
      ))

      expect(button.find('button').prop('type')).toEqual('button')
    })
    it('has a FontAwesomeIcon inside', () => {
      const button = shallow((
        <Button {...props}>
          <FontAwesomeIcon />
        </Button>
      ))

      expect(button.find(FontAwesomeIcon).length).toEqual(1)
    })
    it('applies any valid HTML prop to the button', () => {
      const button = shallow((
        <Button {...props} disabled>
          <FontAwesomeIcon />
        </Button>
      ))

      expect(button.prop('disabled')).toBeTruthy()
    })
    it('appends a className to the default classNames', () => {
      const def = shallow((
        <Button {...props}>
          <FontAwesomeIcon />
        </Button>
      ))
      const classNames = def.prop('className')

      const button = shallow((
        <Button {...props} className='added'>
          <FontAwesomeIcon />
        </Button>
      ))

      expect(button.prop('className')).toEqual(`${classNames} added`)
    })
    it('sets the btn-info className if active', () => {
      const button = shallow((
        <Button {...props} active={true}>
          <FontAwesomeIcon />
        </Button>
      ))

      expect(button.prop('className')).toMatch(/btn-info/)
    })
  })
})
