import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { shallow } from 'enzyme'

import { filters } from '../../actions/badus'
import Filter from './Filter'

const props = {
  dispatch: jest.fn(),
  badus: {
    ids: [],
    visible: [],
  },
}

describe('<UI/Buttons/Filter />', () => {
  describe('::render', () => {
    it('has a FontAwesomeIcon inside', () => {
      const button = shallow(<Filter {...props} />)

      expect(button.find(FontAwesomeIcon).length).toEqual(1)
    })
    it('has a title prop set', () => {
      const button = shallow(<Filter {...props} />)

      expect(button.prop('title')).toBeDefined()
      expect(button.prop('title')).toEqual('Filter closed')
    })
  })
  describe('::onClick', () => {
    it('dispatches toggleClosed', () => {
      const button = shallow(<Filter {...props} />)

      button.simulate('click')
      expect(props.dispatch).toHaveBeenCalled()
      expect(props.dispatch.mock.calls[0][0]).toEqual(filters.toggleClosed())
    })
  })
})
