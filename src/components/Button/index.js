import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { childrenOfType } from 'airbnb-prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import classNames from 'classnames'
import cleanProps from 'clean-react-props'

class Button extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      childrenOfType(FontAwesomeIcon),
      PropTypes.string,
    ]).isRequired,
    active: PropTypes.bool,
  }

  render() {
    const { children } = this.props

    const className = ['btn btn-sm btn-filter']

    if (this.props.active) {
      className.push('btn-info')
    } else {
      className.push('btn-dark')
    }

    if (this.props.className) {
      className.push(this.props.className)
    }

    return (
      <button
        type='button'
        {...cleanProps(this.props)}
        className={classNames(className)}
      >
        {children}
      </button>
    )
  }
}

export default Button
