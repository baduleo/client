import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { shallow } from 'enzyme'

import Add from './Add'

const props = {
  dispatch: jest.fn(),
  form: {
    active: true,
  },
}

describe('<Buttons/Add />', () => {
  describe('::render', () => {
    it('has a FontAwesomeIcon inside', () => {
      const button = shallow(<Add {...props} />)

      expect(button.find(FontAwesomeIcon).length).toEqual(1)
    })
    it('has a title prop set', () => {
      const button = shallow(<Add {...props} />)

      expect(button.prop('title')).toBeDefined()
      expect(button.prop('title')).toEqual('Add new badu')
    })
  })
  describe('::onClick', () => {
    it('dispatches ...', () => {
      const button = shallow(<Add {...props} />)

      button.simulate('click')
      expect(props.dispatch).toHaveBeenCalled()
    })
  })
})
