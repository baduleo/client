import React from 'react'
import { shallow } from 'enzyme'

import { defaultBadusState } from '../../reducers/badus'
import { defaultFormState } from '../../reducers/form'

import Submit from './Submit'

const props = {
  dispatch: jest.fn(),
  badus: defaultBadusState,
  form: {
    ...defaultFormState,
    active: true,
  },
}

describe('<Form/Submit />', () => {
  describe('::render', () => {
    it('renders as expected', () => {
      const component = shallow(<Submit {...props} />)

      expect(component.find('.btn-submit').length).toBe(1)
    })
  })
  describe('::onSubmit', () => {
    it('calls dispatch(post) on click the submit button', () => {
      const spied = {
        ...props,
        dispatch: jest.fn(),
      }

      const component = shallow(<Submit {...spied} />)

      component.simulate('click', {
        preventDefault: jest.fn(),
      })
      expect(spied.dispatch).toHaveBeenCalled()
      expect(typeof spied.dispatch.mock.calls[0][0]).toEqual('function')
    })
  })
})
