import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlusSquare } from '@fortawesome/free-solid-svg-icons'

import { toggle } from '../../actions/form'
import Button from '.'

class Add extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    form: PropTypes.shape({
      active: PropTypes.bool.isRequired,
    }).isRequired,
  }

  onClick() {
    this.props.dispatch(toggle())
  }

  render() {
    const { form: { active } } = this.props

    return (
      <Button
        title='Add new badu'
        onClick={this.onClick.bind(this)}
        active={active}
      >
        <FontAwesomeIcon icon={faPlusSquare} />
      </Button>
    )
  }
}

export default Add
