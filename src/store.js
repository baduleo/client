import {
  createStore as createReduxStore,
  applyMiddleware,
  compose,
} from 'redux'
import thunk from 'redux-thunk'
import { routerMiddleware } from 'react-router-redux'
import createHistory from 'history/createBrowserHistory'

import reducers from './reducers'

export const history = createHistory()

export const createStore = () => {
  const enhancers = []
  const middleware = [
    thunk,
    routerMiddleware(history),
  ]

  if (process.env.NODE_ENV === 'development') {
    const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__

    if (typeof devToolsExtension === 'function') {
      enhancers.push(devToolsExtension())
    }
  }

  const composedEnhancers = compose(
    applyMiddleware(...middleware),
    ...enhancers
  )

  // Prepare store with all the middlewares
  const store = createReduxStore(
    reducers,
    composedEnhancers,
  )

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('./reducers', () => {
      const nextReducer = require('./reducers')
      store.replaceReducer(nextReducer)
    })
  }

  return store
}


const store = createStore()

export default store
