import createStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import { FORM_OPEN, FORM_CLOSE, FORM_FIELD_UPDATE, FORM_LOCATION_SOURCE_SET } from '../constants'
import { toggle, open, close, update, setLocationSource } from './form'

const mockStore = createStore([thunk])

describe('actions::map', () => {
  it('fires FORM_OPEN', () => {
    const store = mockStore()
    store.dispatch(open())

    expect(store.getActions()).toEqual([{
      type: FORM_OPEN,
    }])
  })
  it('fires FORM_CLOSE', () => {
    const store = mockStore()
    store.dispatch(close())

    expect(store.getActions()).toEqual([{
      type: FORM_CLOSE,
    }])
  })
  it('fires FORM_FIELD_UPDATE', () => {
    const store = mockStore()
    store.dispatch(update('field', 'value'))

    expect(store.getActions()).toEqual([{
      type: FORM_FIELD_UPDATE,
      field: 'field',
      value: 'value',
    }])
  })
  describe('::setLocationSource', () => {
    const mapstate = {
      map: {
        currentCenter: {
          lat: 23,
          lng: 32,
        },
      },
      geolocation: {
        watch: {
          latitude: 221,
          longitude: 231,
        },
      },
    }
    it('should fire FORM_LOCATION_SOURCE_SET', () => {
      const store = mockStore(mapstate)
      const expected = [{
        type: FORM_LOCATION_SOURCE_SET,
        source: 'map',
        center: mapstate.map.currentCenter,
      }]
      store.dispatch(setLocationSource('map'))

      expect(store.getActions()).toEqual(expected)
    })
    it('should take currentCenter as default center value', () => {
      const store = mockStore(mapstate)
      const expected = [{
        type: FORM_LOCATION_SOURCE_SET,
        source: 'map',
        center: mapstate.map.currentCenter,
      }]
      store.dispatch(setLocationSource('map'))

      expect(store.getActions()).toEqual(expected)
    })
    it('should take the center from geolocation watch in case source === watch', () => {
      const store = mockStore(mapstate)
      const expected = [{
        type: FORM_LOCATION_SOURCE_SET,
        source: 'watch',
        center: {
          lat: mapstate.geolocation.watch.latitude,
          lng: mapstate.geolocation.watch.longitude,
        },
      }]
      store.dispatch(setLocationSource('watch'))

      expect(store.getActions()).toEqual(expected)
    })
  })
  describe('::toggle', () => {
    it('fires FORM_OPEN if state.form.active === false', () => {
      const store = mockStore({
        form: {
          active: false,
        },
      })
      store.dispatch(toggle())

      expect(store.getActions()).toEqual([{
        type: FORM_OPEN,
      }])
    })
    it('fires FORM_CLOSE if state.form.active === true', () => {
      const store = mockStore({
        form: {
          active: true,
        },
      })
      store.dispatch(toggle())

      expect(store.getActions()).toEqual([{
        type: FORM_CLOSE,
      }])
    })
  })
})
