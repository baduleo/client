import createStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import axiosCore from 'axios'
import MockAdapter from 'axios-mock-adapter'

import {
  BADUS_FETCH,
  BADUS_FETCH_SUCCESS,
  BADUS_FILTERS_CLEAR,
  BADUS_FILTERS_OPEN,
  BADUS_FILTERS_ALL,
  BADUS_TOGGLE_OPEN,
  BADUS_FETCH_ERROR,
  BADUS_FILTERS_CLOSED,
  BADUS_POST,
  BADUS_POST_SUCCESS,
  BADUS_POST_ERROR,
  BADUS_VALIDATION_ERROR,
} from '../constants'
import badus from '../badus.json'

import { fetch, filters, post } from './badus'


const axios = new MockAdapter(axiosCore)
const mockStore = createStore([thunk])

afterEach(() => {
  axios.reset()
})

describe('actions::badus', () => {
  describe('::fetch', () => {
    it('should fire FETCH and SUCCESS if success', async() => {
      axios.onGet(/\/badus$/).reply(200, badus)
      const store = mockStore()

      const expected = [
        {
          type: BADUS_FETCH,
        },
        {
          type: BADUS_FETCH_SUCCESS,
          badus,
        },
      ]

      await store.dispatch(fetch()).then(() => {
        expect(store.getActions()).toEqual(expected)
      })
    })

    it('should fire FETCH and ERROR if error', async() => {
      const error = 'Error: Request failed with status code 400'
      axios.onGet(/\/badus$/).reply(404, error)
      const store = mockStore()

      const expected = [
        {
          type: BADUS_FETCH,
        },
        {
          type: BADUS_FETCH_ERROR,
        },
      ]

      await store.dispatch(fetch()).then(() => {
        const result = store.getActions()

        expect(result[1].error).toBeDefined()

        delete result[1].error
        expect(result).toEqual(expected)
      })
    })
  })

  describe('::post', () => {
    it('should fire POST and SUCCESS if success', async() => {
      axios.onPost(/\/badus$/).reply(201, badus[0])
      const store = mockStore()

      const expected = [
        {
          type: BADUS_POST,
        },
        {
          type: BADUS_POST_SUCCESS,
          badu: badus[0],
        },
      ]

      await store.dispatch(post()).then(() => {
        expect(store.getActions()).toEqual(expected)
      })
    })

    it('should fire POST and ERROR if error', async() => {
      axios.onPost(/\/badus$/).reply(404)
      const store = mockStore()

      const expected = [
        {
          type: BADUS_POST,
        },
        {
          type: BADUS_POST_ERROR,
          error: undefined,
        },
      ]

      await store.dispatch(post()).then(() => {
        const result = store.getActions()

        expect(result).toEqual(expected)
      })
    })

    it('should fire POST and VALIDATION_ERROR if validation error', async() => {
      const error = {
        name: {
          error: 'test',
        },
      }
      axios.onPost(/\/badus$/).reply(400, error)
      const store = mockStore()

      const expected = [
        {
          type: BADUS_POST,
        },
        {
          type: BADUS_VALIDATION_ERROR,
          error,
        },
      ]

      await store.dispatch(post()).then(() => {
        const result = store.getActions()

        expect(result).toEqual(expected)
      })
    })
  })

  describe('::filters', () => {
    it('::all should fire ALL', () => {
      const store = mockStore()

      const expected = [
        {
          type: BADUS_FILTERS_ALL,
        },
      ]

      store.dispatch(filters.all())
      expect(store.getActions()).toEqual(expected)
    })

    it('::clear should fire CLEAR', () => {
      const store = mockStore()

      const expected = [
        {
          type: BADUS_FILTERS_CLEAR,
        },
      ]

      store.dispatch(filters.clear())
      expect(store.getActions()).toEqual(expected)
    })

    it('::open should fire OPEN', () => {
      const store = mockStore()

      const expected = [
        {
          type: BADUS_FILTERS_OPEN,
        },
      ]

      store.dispatch(filters.open())
      expect(store.getActions()).toEqual(expected)
    })

    it('::close should fire CLOSE', () => {
      const store = mockStore()

      const expected = [
        {
          type: BADUS_FILTERS_CLOSED,
        },
      ]

      store.dispatch(filters.closed())
      expect(store.getActions()).toEqual(expected)
    })

    it('::toggleOpen should fire TOGGLE_OPEN', () => {
      const store = mockStore()

      const expected = [
        {
          type: BADUS_TOGGLE_OPEN,
        },
      ]

      store.dispatch(filters.toggleOpen())
      expect(store.getActions()).toEqual(expected)
    })
  })
})
