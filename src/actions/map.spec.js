import createStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import {
  MAP_SET_CENTER,
  MAP_POPUP_OPEN,
  MAP_POPUP_CLOSE,
  MAP_ZOOM_SET,
  MAP_ZOOM_INCREASE,
  MAP_ZOOM_DECREASE,
  MAP_SET_CURRENT_CENTER,
} from '../constants'

import {
  setCenter,
  openPopup,
  closePopup,
  setZoom,
  increaseZoom,
  decreaseZoom,
  setCurrentCenter,
} from './map'

const mockStore = createStore([thunk])

describe('actions::map', () => {
  describe('::setCenter', () => {
    const store = mockStore()
    it('fires MAP_SET_CENTER', () => {
      store.dispatch(setCenter('test'))

      expect(store.getActions()).toEqual([{
        type: MAP_SET_CENTER,
        center: 'test',
      }])
    })
  })

  describe('::setCurrentCenter', () => {
    const store = mockStore()
    it('fires MAP_SET_CURRENT_CENTER', () => {
      store.dispatch(setCurrentCenter('test'))

      expect(store.getActions()).toEqual([{
        type: MAP_SET_CURRENT_CENTER,
        center: 'test',
      }])
    })
  })

  describe('::openPopup', () => {
    const store = mockStore()
    it('fires MAP_POPUP_OPEN', () => {
      store.dispatch(openPopup('test'))

      expect(store.getActions()).toEqual([{
        type: MAP_POPUP_OPEN,
        info: 'test',
      }])
    })
  })

  describe('::closePopup', () => {
    const store = mockStore()
    it('fires MAP_POPUP_CLOSE', () => {
      store.dispatch(closePopup())

      expect(store.getActions()).toEqual([{
        type: MAP_POPUP_CLOSE,
      }])
    })
  })

  describe('::setZoom', () => {
    const store = mockStore()
    it('fires MAP_ZOOM_SET', () => {
      store.dispatch(setZoom(23))

      expect(store.getActions()).toEqual([{
        type: MAP_ZOOM_SET,
        zoom: 23,
      }])
    })
  })

  describe('::increaseZoom', () => {
    const store = mockStore()
    it('fires MAP_ZOOM_INCREASE', () => {
      store.dispatch(increaseZoom(2))

      expect(store.getActions()).toEqual([{
        type: MAP_ZOOM_INCREASE,
        amount: 2,
      }])
    })
  })

  describe('::decreaseZoom', () => {
    const store = mockStore()
    it('fires MAP_ZOOM_DECREASE', () => {
      store.dispatch(decreaseZoom(3))

      expect(store.getActions()).toEqual([{
        type: MAP_ZOOM_DECREASE,
        amount: 3,
      }])
    })
  })
})
