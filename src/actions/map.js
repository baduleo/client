import {
  MAP_SET_CENTER,
  MAP_POPUP_OPEN,
  MAP_POPUP_CLOSE,
  MAP_ZOOM_SET,
  MAP_ZOOM_INCREASE,
  MAP_ZOOM_DECREASE,
  MAP_SET_CURRENT_CENTER,
} from '../constants'

export const setCenter = (center) => ({
  type: MAP_SET_CENTER,
  center,
})
export const setCurrentCenter = (center) => ({
  type: MAP_SET_CURRENT_CENTER,
  center,
})
export const openPopup = (info) => ({
  type: MAP_POPUP_OPEN,
  info,
})
export const closePopup = () => ({
  type: MAP_POPUP_CLOSE,
})
export const setZoom = (zoom) => ({
  type: MAP_ZOOM_SET,
  zoom,
})
export const increaseZoom = (amount) => ({
  type: MAP_ZOOM_INCREASE,
  amount,
})
export const decreaseZoom = (amount) => ({
  type: MAP_ZOOM_DECREASE,
  amount,
})
