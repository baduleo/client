import {
  FORM_CLOSE,
  FORM_OPEN,
  FORM_FIELD_UPDATE,
  FORM_LOCATION_SOURCE_SET,
} from '../constants'

export const open = (center) => ({
  type: FORM_OPEN,
  center,
})

export const close = (center) => ({
  type: FORM_CLOSE,
  center,
})

export const update = (field, value) => ({
  type: FORM_FIELD_UPDATE,
  field,
  value,
})

export const setLocationSource = (source) => (dispatch, getState) => {
  const center = {
    ...getState().map.currentCenter,
  }

  if (source === 'watch') {
    const watch = getState().geolocation.watch
    center.lat = watch.latitude
    center.lng = watch.longitude
  }

  return dispatch({
    type: FORM_LOCATION_SOURCE_SET,
    source,
    center,
  })
}

export const toggle = () => (dispatch, getState) => {
  const { form: { active }} = getState()

  if (active) {
    return dispatch(close())
  }

  return dispatch(open())
}
