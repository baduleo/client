import axios from 'axios'
import {
  BADUS_FETCH,
  BADUS_FETCH_ERROR,
  BADUS_FETCH_SUCCESS,
  BADUS_FILTERS_CLEAR,
  BADUS_FILTERS_OPEN,
  BADUS_FILTERS_CLOSED,
  BADUS_FILTERS_ALL,
  BADUS_TOGGLE_OPEN,
  BADUS_TOGGLE_CLOSED,
  BADUS_POST,
  BADUS_POST_ERROR,
  BADUS_POST_SUCCESS,
  BADUS_VALIDATION_ERROR,
} from '../constants'

import { processResponse } from '../utils'

export const badus = {
  fetch: () => ({
    type: BADUS_FETCH,
  }),
  fetchError: (error) => ({
    type: BADUS_FETCH_ERROR,
    error,
  }),
  fetchSuccess: (badus) => ({
    type: BADUS_FETCH_SUCCESS,
    badus,
  }),
  post: () => ({
    type: BADUS_POST,
  }),
  postValidationError: (error) => ({
    type: BADUS_VALIDATION_ERROR,
    error,
  }),
  postError: (error) => ({
    type: BADUS_POST_ERROR,
    error,
  }),
  postSuccess: (badu) => ({
    type: BADUS_POST_SUCCESS,
    badu,
  }),
}

export const filters = {
  all: () => ({
    type: BADUS_FILTERS_ALL,
  }),
  clear: () => ({
    type: BADUS_FILTERS_CLEAR,
  }),
  open: () => ({
    type: BADUS_FILTERS_OPEN,
  }),
  toggleOpen: () => ({
    type: BADUS_TOGGLE_OPEN,
  }),
  toggleClosed: () => ({
    type: BADUS_TOGGLE_CLOSED,
  }),
  closed: () => ({
    type: BADUS_FILTERS_CLOSED,
  }),
}

export const fetch = () => (dispatch) => {
  dispatch(badus.fetch())

  return axios.get(`${process.env.API_URL}/badus`)
    .then(processResponse)
    .then((result) => dispatch(badus.fetchSuccess(result)))
    .catch((error) => dispatch(badus.fetchError(error)))
}

export const post = (data) => (dispatch) => {
  dispatch(badus.post())

  return axios.post(`${process.env.API_URL}/badus`, data)
    .then(processResponse)
    .then((result) => dispatch(badus.postSuccess(result)))
    .catch(({ response }) => {
      if (response.status === 400) {
        return dispatch(badus.postValidationError(response.data))
      }

      return dispatch(badus.postError(response.statusText))
    })
}
