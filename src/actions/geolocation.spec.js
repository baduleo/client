import createStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import axiosCore from 'axios'
import MockAdapter from 'axios-mock-adapter'

import {
  GEO_LOCATION,
  GEO_LOCATION_SUCCESS,
  GEO_LOCATION_ERROR,
  GEO_LOCATION_WATCH,
  GEO_LOCATION_WATCH_SUCCESS,
  GEO_LOCATION_WATCH_ERROR,
  GEO_QUERY,
  GEO_QUERY_SUCCESS,
  GEO_QUERY_ERROR,
} from '../constants'
import {
  locate,
  locateSuccess,
  locateError,
  watch,
  watchSuccess,
  watchError,
  query,
} from './geolocation'

const mockStore = createStore([thunk])
const axios = new MockAdapter(axiosCore)

afterEach(() => axios.reset())

describe('actions::geolocation', () => {
  describe('::locate', () => {
    it('should fire GEO_LOCATION', () => {
      const store = mockStore({})

      const expected = [
        {
          type: GEO_LOCATION,
        },
      ]

      store.dispatch(locate())
      expect(store.getActions()).toEqual(expected)
    })
  })
  describe('::locateSuccess', () => {
    it('should fire GEO_LOCATION_SUCCESS', () => {
      const store = mockStore({})

      const expected = [
        {
          type: GEO_LOCATION_SUCCESS,
        },
      ]

      store.dispatch(locateSuccess())
      expect(store.getActions()).toEqual(expected)
    })
  })
  describe('::locateError', () => {
    it('should fire GEO_LOCATION_ERROR', () => {
      const store = mockStore({})

      const expected = [
        {
          type: GEO_LOCATION_ERROR,
        },
      ]

      store.dispatch(locateError())
      expect(store.getActions()).toEqual(expected)
    })
  })
  describe('::watch', () => {
    it('should fire GEO_LOCATION', () => {
      const store = mockStore({})

      const expected = [
        {
          type: GEO_LOCATION_WATCH,
        },
      ]

      store.dispatch(watch())
      expect(store.getActions()).toEqual(expected)
    })
  })
  describe('::watchSuccess', () => {
    it('should fire GEO_LOCATION_SUCCESS', () => {
      const store = mockStore({})

      const expected = [
        {
          type: GEO_LOCATION_WATCH_SUCCESS,
        },
      ]

      store.dispatch(watchSuccess())
      expect(store.getActions()).toEqual(expected)
    })
  })
  describe('::watchError', () => {
    it('should fire GEO_LOCATION_ERROR', () => {
      const store = mockStore({})

      const expected = [
        {
          type: GEO_LOCATION_WATCH_ERROR,
        },
      ]

      store.dispatch(watchError())
      expect(store.getActions()).toEqual(expected)
    })
  })

  describe('::query', () => {
    it('fires GEO_QUERY & GEO_QUERY_SUCCESS on success call', async() => {
      axios.onGet(/mapbox\.places/).reply(200, {features: []})
      const store = mockStore({})
      const expected = [
        {
          type: GEO_QUERY,
        },
        {
          type: GEO_QUERY_SUCCESS,
          results: [],
        },
      ]

      await store.dispatch(query('41,1.23')).then(() => {
        expect(store.getActions()).toEqual(expected)
      })
    })
    it('fires GEO_QUERY & GEO_QUERY_ERROR on error call', async() => {
      axios.onGet(/mapbox\.places/).reply(404)
      const store = mockStore({})
      const expected = [
        {
          type: GEO_QUERY,
        },
        {
          type: GEO_QUERY_ERROR,
          error: new Error('Request failed with status code 404'),
        },
      ]

      await store.dispatch(query('41,1.23')).then(() => {
        expect(store.getActions()).toEqual(expected)
      })
    })
  })
})
