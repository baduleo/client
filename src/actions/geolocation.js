import axios from 'axios'

import { processResponse } from '../utils'
import {
  GEO_LOCATION,
  GEO_LOCATION_SUCCESS,
  GEO_LOCATION_ERROR,
  GEO_LOCATION_WATCH,
  GEO_LOCATION_WATCH_SUCCESS,
  GEO_LOCATION_WATCH_ERROR,
  GEO_QUERY,
  GEO_QUERY_ERROR,
  GEO_QUERY_SUCCESS,
  MAPBOX_API_PLACES,
} from '../constants'


export const locate = () => ({
  type: GEO_LOCATION,
})

export const locateSuccess = (payload) => ({
  type: GEO_LOCATION_SUCCESS,
  payload,
})

export const locateError = (error) => ({
  type: GEO_LOCATION_ERROR,
  error,
})

export const watch = () => ({
  type: GEO_LOCATION_WATCH,
})

export const watchSuccess = (payload) => ({
  type: GEO_LOCATION_WATCH_SUCCESS,
  payload,
})

export const watchError = (error) => ({
  type: GEO_LOCATION_WATCH_ERROR,
  error,
})

export const queryActions = {
  query: () => ({
    type: GEO_QUERY,
  }),
  success: ({ features }) => ({
    type: GEO_QUERY_SUCCESS,
    results: features,
  }),
  error: (error) => ({
    type: GEO_QUERY_ERROR,
    error,
  }),
}

export const query = (textOrCoords) => (dispatch) => {
  dispatch(queryActions.query())

  return axios.get(`${MAPBOX_API_PLACES}/${textOrCoords}.json?access_token=${process.env.MAPBOX_TOKEN}`)
    .then(processResponse)
    .then((response) => dispatch(queryActions.success(response)))
    .catch((error) => dispatch(queryActions.error(error)))
}
