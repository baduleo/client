import {
  GEO_LOCATION_SUCCESS,
  GEO_LOCATION_ERROR,
  GEO_LOCATION_WATCH_SUCCESS,
  GEO_LOCATION_WATCH_ERROR,
} from '../constants'

const locationObject = {
  latitude: null,
  longitude: null,
  error: false,
  altitude: null,
  accuracy: null,
  altitudeAccuracy: null,
  heading: null,
  speed: null,
}

export const defaultGeolocationState = {
  watch: {...locationObject},
  locate: {...locationObject},
}

const geolocation = (state = defaultGeolocationState, action = {}) => {
  switch (action.type) {
    case GEO_LOCATION_SUCCESS:
      return {
        ...state,
        locate: {
          ...state.locate,
          ...action.payload,
        },
      }

    case GEO_LOCATION_ERROR:
      return {
        ...state,
        locate: {
          ...state.locate,
          error: action.error,
        },
      }

    case GEO_LOCATION_WATCH_SUCCESS:
      return {
        ...state,
        watch: {
          ...state.watch,
          ...action.payload,
        },
      }

    case GEO_LOCATION_WATCH_ERROR:
      return {
        ...state,
        watch: {
          ...state.watch,
          error: action.error,
        },
      }

    default:
      return state
  }
}

export default geolocation
