import { createStore } from 'redux'
import { routerReducer } from 'react-router-redux'

import form from './form'
import badus from './badus'
import geolocation from './geolocation'
import reducers from '.'

const store = createStore(reducers)

describe('reducers::index', () => {
  it('initial state checks', () => {
    expect(store.getState().router).toEqual(routerReducer(undefined, {}))
    expect(store.getState().form).toEqual(form(undefined, {}))
    expect(store.getState().badus).toEqual(badus(undefined, {}))
    expect(store.getState().geolocation).toEqual(geolocation(undefined, {}))
  })
})
