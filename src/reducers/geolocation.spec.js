import {
  locateError,
  watchError,
  locateSuccess,
  watchSuccess,
} from '../actions/geolocation'
import geolocation, { defaultGeolocationState } from './geolocation'

describe('reducers::geolocation', () => {
  it('returns the default state if action is unknown', () => {
    expect(geolocation(undefined, {})).toEqual(defaultGeolocationState)
  })
  const location = {
    latitude: 23,
    longitude: 32,
    altitude: 1000,
  }
  it('populates locate with payload in GEO_LOCATION_SUCCESS', () => {
    expect(geolocation(undefined, locateSuccess(location))).toEqual({
      ...defaultGeolocationState,
      locate: {
        ...defaultGeolocationState.locate,
        ...location,
      },
    })
  })
  it('populates watch with payload in GEO_LOCATION_WATCH_SUCCESS', () => {
    expect(geolocation(undefined, watchSuccess(location))).toEqual({
      ...defaultGeolocationState,
      watch: {
        ...defaultGeolocationState.locate,
        ...location,
      },
    })
  })
  it('populates locate.error if action is GEO_LOCATION_ERROR', () => {
    expect(geolocation(undefined, locateError('error'))).toEqual({
      ...defaultGeolocationState,
      locate: {
        ...defaultGeolocationState.locate,
        error: 'error',
      },
    })
  })
  it('populates watch.error if action is GEO_LOCATION_WATCH_ERROR', () => {
    expect(geolocation(undefined, watchError('error'))).toEqual({
      ...defaultGeolocationState,
      watch: {
        ...defaultGeolocationState.watch,
        error: 'error',
      },
    })
  })
})
