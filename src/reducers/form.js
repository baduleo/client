import {
  FORM_OPEN,
  FORM_CLOSE,
  FORM_FIELD_UPDATE,
  BADUS_POST_SUCCESS,
  FORM_LOCATION_SOURCE_SET,
  MAP_SET_CURRENT_CENTER,
  GEO_LOCATION_WATCH_SUCCESS,
  BADUS_POST_ERROR,
  BADUS_VALIDATION_ERROR,
  BADUS_POST,
  GEO_QUERY_SUCCESS,
  GEO_QUERY,
} from '../constants'

export const defaultFormState = {
  active: false,
  loading: false,
  errors: false,
  locationSource: 'watch', // can either be watch, map or custom
  form: {},
}

const reset = (state) => {
  const form = {
    lat: state.form.lat,
    lng: state.form.lng,
  }

  return {
    ...state,
    active: false,
    loading: false,
    errors: false,
    form,
  }
}

const loading = (state) => ({
  ...state,
  loading: true,
  errors: false,
})

const onError = (state, { error }) => {
  return {
    ...state,
    loading: false,
    errors: error,
  }
}


const updateCoordinates = (state, action) => {
  if (action.type === MAP_SET_CURRENT_CENTER && state.locationSource === 'map') {
    return {
      ...state,
      form: {
        ...state.form,
        ...action.center,
      },
    }
  }

  if (action.type === GEO_LOCATION_WATCH_SUCCESS && state.locationSource === 'watch') {
    return {
      ...state,
      form: {
        ...state.form,
        lat: action.payload.latitude,
        lng: action.payload.longitude,
      },
    }
  }

  return state
}

const form = (state = defaultFormState, action = {}) => {
  switch(action.type) {
    // This is not a good idea, but is somethin temporal (I hope)
    case GEO_LOCATION_WATCH_SUCCESS:
    case MAP_SET_CURRENT_CENTER:
      return updateCoordinates(state, action)

    case GEO_QUERY:
      return loading(state)

    case GEO_QUERY_SUCCESS:
      return {
        ...state,
        loading: false,
        form: {
          ...state.form,
          address: action.results[0].place_name,
        },
      }

    case BADUS_POST:
      return {
        ...state,
        loading: true,
        errors: false,
      }

    case FORM_FIELD_UPDATE:
      return {
        ...state,
        form: {
          ...state.form,
          [action.field]: action.value,
        },
      }

    case FORM_OPEN:
      return {
        ...state,
        active: true,
      }

    case BADUS_POST_SUCCESS:
    case FORM_CLOSE:
      return reset(state)

    case BADUS_POST_ERROR:
    case BADUS_VALIDATION_ERROR:
      return onError(state, action)

    case FORM_LOCATION_SOURCE_SET:
      return {
        ...state,
        locationSource: action.source,
        form: {
          ...state.form,
          ...action.center,
        },
      }

    default:
      return state
  }
}

export default form
