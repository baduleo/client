import { badus as actions, filters } from '../actions/badus'

import badus, { defaultBadusState } from './badus'

describe('reducers::badus', () => {
  it('returns the default state if action is unknown', () => {
    expect(badus(undefined, {})).toEqual(defaultBadusState)
  })
  describe('::loading', () => {
    const state = {
      ...defaultBadusState,
      loading: false,
      error: 'testing',
    }
    const expected = {
      ...defaultBadusState,
      loading: true,
      error: false,
    }
    it('gets fired on BADUS_FETCH', () => {
      expect(badus(state, actions.fetch())).toEqual(expected)
    })
    it('sets loading and removes errors', () => {
      expect(badus(state, actions.fetch())).toEqual(expected)
    })
  })
  describe('::onFetchSuccess', () => {
    const data = [
      {_id: 1, name: 'badu'},
      {_id: 2, name: 'badu 2'},
    ]
    const state = {
      ...defaultBadusState,
      loading: true,
    }
    const expected = {
      ...defaultBadusState,
      loading: false,
      ids: [1, 2],
      visible: [1, 2],
      all: {
        1: {_id: 1, name: 'badu'},
        2: {_id: 2, name: 'badu 2'},
      },
    }
    it('gets called on BADUS_FETCH_SUCCESS', () => {
      expect(badus(state, actions.fetchSuccess(data))).toEqual(expected)
    })
    it('sets visible, ids, and all values', () => {
      expect(badus(state, actions.fetchSuccess(data))).toEqual(expected)
    })
  })
  it('clears filters on BADUS_FILTERS_CLEAR', () => {
    const state = {
      ...defaultBadusState,
      ids: [1, 2, 3, 4, 5],
      visible: [1, 2],
    }
    const expected = {
      ...defaultBadusState,
      ids: [1, 2, 3, 4, 5],
      visible: [1, 2, 3, 4, 5],
    }
    expect(badus(state, filters.clear())).toEqual(expected)
  })
  it('BADUS_FILTERS_ALL filters all', () => {
    const state = {
      ...defaultBadusState,
      visible: [1, 2, 3],
    }
    const expected = {
      ...defaultBadusState,
      visible: [],
    }
    expect(badus(state, filters.all())).toEqual(expected)
  })
})
