import { locateSuccess } from '../actions/geolocation'
import {
  setCenter,
  openPopup,
  closePopup,
  setZoom,
  increaseZoom,
  decreaseZoom,
  setCurrentCenter,
} from '../actions/map'
import { ZOOM_ON_POPUP } from '../constants'

import map, { defaultMapState } from './map'

describe('reducers::map', () => {
  it('returns the default state if action is unknown', () => {
    expect(map(undefined, {})).toEqual(defaultMapState)
  })

  it('sets center on GEO_LOCATION_SUCCESS', () => {
    const location = {
      latitude: 23,
      longitude: 32,
    }

    const expected = {
      ...defaultMapState,
      center: [
        location.longitude,
        location.latitude,
      ],
    }

    expect(map(undefined, locateSuccess(location))).toEqual(expected)

    expect(map(undefined, locateSuccess({}))).toEqual({
      ...expected,
      center: defaultMapState.center,
    })
  })

  it('sets center on MAP_SET_CENTER', () => {
    const location = [23, 32]
    const expected = {
      ...defaultMapState,
      center: location,
    }
    expect(map({...defaultMapState}, setCenter(location))).toEqual(expected)
  })

  it('sets center on MAP_SET_CURRENT_CENTER', () => {
    const location = [23, 32]
    const expected = {
      ...defaultMapState,
      currentCenter: location,
    }
    expect(map({...defaultMapState}, setCurrentCenter(location))).toEqual(expected)
  })

  it('sets popup info on MAP_POPUP_OPEN and zooms (only when required)', () => {
    const expected = {
      ...defaultMapState,
      popup: 'test',
      zoom: ZOOM_ON_POPUP,
    }
    expect(map({...defaultMapState}, openPopup('test'))).toEqual(expected)

    expect(map({
      ...defaultMapState,
      zoom: 17,
    }, openPopup('test'))).toEqual({
      ...expected,
      zoom: 17,
    })
  })

  it('removes popup info on MAP_POPUP_CLOSE', () => {
    const expected = {
      ...defaultMapState,
    }
    expect(map({
      ...defaultMapState,
      popup: 'test',
    },
    closePopup())).toEqual(expected)
  })

  it('sets zoom on MAP_SET_ZOOM', () => {
    const expected = {
      ...defaultMapState,
      zoom: 23,
    }
    expect(map({
      ...defaultMapState,
    },
    setZoom(23))).toEqual(expected)
  })

  it('increases zoom on MAP_ZOOM_INCREASE', () => {
    const expected = {
      ...defaultMapState,
      zoom: 23,
    }
    expect(map({
      ...defaultMapState,
      zoom: 21,
    },
    increaseZoom(2))).toEqual(expected)
  })

  it('decreases zoom on MAP_ZOOM_DECREASE', () => {
    const expected = {
      ...defaultMapState,
      zoom: 23,
    }
    expect(map({
      ...defaultMapState,
      zoom: 25,
    },
    decreaseZoom(2))).toEqual(expected)
  })
})
