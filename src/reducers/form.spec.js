import { open, close, update } from '../actions/form'

import {
  BADUS_POST_SUCCESS,
  FORM_LOCATION_SOURCE_SET,
  MAP_SET_CURRENT_CENTER,
  GEO_LOCATION_WATCH_SUCCESS,
} from '../constants'
import form, { defaultFormState } from './form'

describe('reducers::badus', () => {
  it('returns the default state if action is unknown', () => {
    expect(form(undefined, {})).toEqual(defaultFormState)
  })
  it('sets active on FORM_OPEN', () => {
    const state = {
      ...defaultFormState,
      active: false,
    }
    const expected = {
      ...defaultFormState,
      active: true,
    }
    expect(form(state, open())).toEqual(expected)
  })

  it('sets a field with its value on FORM_FIELD_UPDATE', () => {
    const state = {
      ...defaultFormState,
      form: {},
    }
    const expected = {
      ...defaultFormState,
      form: {
        field: 'value',
      },
    }
    expect(form(state, update('field', 'value'))).toEqual(expected)
  })

  it('sets locationSource and form coords on FORM_LOCATION_SOURCE_SET', () => {
    const state = {
      ...defaultFormState,
      form: {},
    }
    const expected = {
      ...defaultFormState,
      locationSource: 'map',
      form: {
        lat: 23,
        lng: 32,
      },
    }
    expect(form(state, {
      type: FORM_LOCATION_SOURCE_SET,
      center: expected.form,
      source: 'map',
    })).toEqual(expected)
  })

  describe('::reset', () => {
    const state = {
      ...defaultFormState,
      active: true,
      form: {
        form: {
          name: 'whatever',
          lat: 23,
          lng:  32,
        },
      },
    }
    const expected = {
      ...defaultFormState,
      active: false,
    }
    it('unsets active on FORM_CLOSE and removes form data', () => {
      expect(form(state, close())).toEqual(expected)
    })
    it('is fired on BADUS_POST_SUCCESS too', () => {
      expect(form(state, {type: BADUS_POST_SUCCESS})).toEqual(expected)
    })
  })

  describe('::updateCoordinates', () => {
    const state = {
      ...defaultFormState,
      locationSource: 'map',
      form: {},
    }
    const expected = {
      ...defaultFormState,
      locationSource: 'map',
      form: {
        lat: 23,
        lng: 32,
      },
    }

    it('does nothing when does not correspond', () => {
      expect(form(state, {type: GEO_LOCATION_WATCH_SUCCESS})).toEqual(state)
    })

    it('sets coords from map on MAP_SET_CURRENT_CENTER && locationSource === map', () => {
      expect(form(state, {
        type: MAP_SET_CURRENT_CENTER,
        center: expected.form,
      })).toEqual(expected)
    })

    it('sets coords from watch on GEO_LOCATION_WATCH_SUCCESS && locationSource === watch', () => {
      const state = {
        ...defaultFormState,
        locationSource: 'watch',
        form: {},
      }
      expect(form(state, {
        type: GEO_LOCATION_WATCH_SUCCESS,
        payload: {
          latitude: expected.form.lat,
          longitude: expected.form.lng,
        },
      })).toEqual({...expected, locationSource: 'watch'})
    })
  })
})
