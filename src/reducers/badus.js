import { ids } from '../utils/mappers'
import {
  BADUS_FETCH,
  BADUS_FETCH_SUCCESS,
  BADUS_FETCH_ERROR,
  BADUS_FILTERS_CLEAR,
  BADUS_FILTERS_OPEN,
  BADUS_FILTERS_CLOSED,
  BADUS_FILTERS_ALL,
  BADUS_TOGGLE_OPEN,
  BADUS_TOGGLE_CLOSED,
  BADUS_POST_SUCCESS,
} from '../constants'
import { isOpen } from '../utils/time'

export const defaultBadusState = {
  all: {},
  ids: [],
  visible: [],
  loading: false,
  error: false,
}

const loading = (state) => ({
  ...state,
  loading: true,
  error: false,
})

const onFetchSuccess = (state, { badus }) => {
  const result = ids(badus, '_id')

  return {
    ...state,
    ...result,
    visible: result.ids,
    loading: false,
  }
}

const onError = (state, { error }) => ({
  ...state,
  loading: false,
  error,
})

const onPostSuccess = (state, { badu }) => {
  const result = {...state}

  result.all[badu.id] = badu
  result.ids.push(badu.id)
  result.visible.push(badu.id)

  return result
}

const clearFilters = (state) => ({
  ...state,
  visible: [...state.ids],
})

const filterOpen = (state) => {
  const open = [...state.ids].filter((id) => {
    const badu = state.all[id]
    const from = badu.from
    const to = badu.to

    return !isOpen(from, to)
  })

  return {
    ...state,
    visible: open,
  }
}

const filterClosed = (state) => {
  const open = [...state.ids].filter((id) => {
    const badu = state.all[id]
    const from = badu.from
    const to = badu.to

    return isOpen(from, to)
  })

  return {
    ...state,
    visible: open,
  }
}

const toggleOpen = (state) => {
  if (state.visible.length === state.ids.length) {
    return filterOpen(state)
  }

  return clearFilters(state)
}

const toggleClosed = (state) => {
  if (state.visible.length === state.ids.length) {
    return filterClosed(state)
  }

  return clearFilters(state)
}

const filterAll = (state) => ({
  ...state,
  visible: [],
})

const badus = (state = defaultBadusState, action = {}) => {
  switch (action.type) {
    case BADUS_FETCH:
      return loading(state)

    case BADUS_FETCH_SUCCESS:
      return onFetchSuccess(state, action)

    case BADUS_POST_SUCCESS:
      return onPostSuccess(state, action)

    case BADUS_FETCH_ERROR:
      return onError(state, action)

    case BADUS_FILTERS_ALL:
      return filterAll(state)

    case BADUS_FILTERS_CLEAR:
      return clearFilters(state, action)

    case BADUS_FILTERS_OPEN:
      return filterOpen(state)

    case BADUS_FILTERS_CLOSED:
      return filterClosed(state)

    case BADUS_TOGGLE_OPEN:
      return toggleOpen(state)

    case BADUS_TOGGLE_CLOSED:
      return toggleClosed(state)

    default:
      return state
  }
}

export default badus
