import {
  MAP_SET_CENTER,
  MAP_POPUP_OPEN,
  MAP_POPUP_CLOSE,
  DEFAULT_ZOOM,
  ZOOM_ON_POPUP,
  MAP_ZOOM_SET,
  FALLBACK_LOCATION,
  MAP_ZOOM_INCREASE,
  MAP_ZOOM_DECREASE,
  GEO_LOCATION_SUCCESS,
  MAP_SET_CURRENT_CENTER,
} from '../constants'


export const defaultMapState = {
  // Updating the center continuously makes weird behaviors.
  center: FALLBACK_LOCATION,
  // that's why it's split. This one stores the real map center.
  currentCenter: null,
  popup: false,
  zoom: DEFAULT_ZOOM,
}

export const openPopup = (state, action) => {
  let zoom = state.zoom
  if (zoom < ZOOM_ON_POPUP) {
    zoom = ZOOM_ON_POPUP
  }

  return {
    ...state,
    popup: action.info,
    zoom,
  }
}

export const zoomIncrease = (state, action) => ({
  ...state,
  zoom: state.zoom + action.amount,
})

export const zoomDecrease = (state, action) => ({
  ...state,
  zoom: state.zoom - action.amount,
})

const map = (state = defaultMapState, action = {}) => {
  switch (action.type) {

    case GEO_LOCATION_SUCCESS: {
      if (!action.payload.latitude || !action.payload.longitude) {
        return state
      }

      return {
        ...state,
        center: [
          action.payload.longitude,
          action.payload.latitude,
        ],
      }
    }

    case MAP_SET_CENTER:
      return {
        ...state,
        center: action.center,
      }

    case MAP_SET_CURRENT_CENTER:
      return {
        ...state,
        currentCenter: action.center,
      }

    case MAP_POPUP_OPEN:
      return openPopup(state, action)

    case MAP_POPUP_CLOSE:
      return {
        ...state,
        popup: false,
      }

    case MAP_ZOOM_SET:
      return {
        ...state,
        zoom: action.zoom,
      }

    case MAP_ZOOM_INCREASE:
      return zoomIncrease(state, action)

    case MAP_ZOOM_DECREASE:
      return zoomDecrease(state, action)

    default:
      return state
  }
}

export default map
