import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import geolocation from './geolocation'
import badus from './badus'
import form from './form'
import map from './map'

const reducers = combineReducers({
  router: routerReducer,
  geolocation,
  badus,
  form,
  map,
})

export default reducers
