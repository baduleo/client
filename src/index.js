import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'

import store, { history } from './store'
import registerServiceWorker from './registerServiceWorker'

import './index.scss'
import Routes from './components/Routes'

render((
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Routes />
    </ConnectedRouter>
  </Provider>
), document.getElementById('root'))

registerServiceWorker()
