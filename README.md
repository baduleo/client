Baduleo React App
=================

[![build][]][build link]
[![coverage][]][coverage link]

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).


Development
-----------

The recommended way to work in this project is with TDD. Any change should be checked in the tests. For that reason I recommend working with the console open and the jest's interactive test.

~~~bash
yarn test --verbose
~~~

Add `--coverage` to also get coverage reports:

~~~bash
yarn test --verbose --coverage
~~~

To start a development server run:

~~~bash
yarn start
~~~

### About geolocation

To be able to properly test geolocation you should run the development server with HTTPS enabled:

~~~bash
HTTPS=true yarn start
~~~

#### Firefox troubleshoot

If firefox fails with an unknown error, try changing the `geo.wifi.uri` key in `about:config` to:

    https://location.services.mozilla.com/v1/geolocate?key=%MOZILLA_API_KEY%

[build]: https://gitlab.com/baduleo/client/badges/master/build.svg
[coverage]: https://gitlab.com/baduleo/client/badges/master/coverage.svg

[build link]: https://gitlab.com/baduleo/client/pipelines
[coverage link]: https://gitlab.com/baduleo/client/-/jobs
